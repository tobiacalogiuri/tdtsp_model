/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bandbtsp;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class Data {
    public int[][] forbidden_arcs;
    public double[][] original_costs;
    //v_ij originale
    double[][] original_v_ij;
    double[][] original_u_ij;    
    //Matrice dei clusters
    public int[][] clusters;
    //matrice t x 2 degli estremi degli istanti temporali
    public double[][] tp;
    //km/min b_h*u_ij cluster x time_periods
    public double[][] speeds;
    //ATSP solution
    public ArrayList<Integer> solution;
    //delta_j
    public double[][] deltas;
        //numero istanti temporali
    public int t = 0;
    //numero clusters
    public int c = 0;
    public int n = 0;
    
    public double [][] tW_td;
    public double max_arrival_time;
    public int max_arrival_time_node;
    public ArrayList<Integer> backup_solution;
    public double z__value;
    ArrayList<Integer>[] Predecessors;
    ArrayList<Integer>[] Successors;
    ArrayList<Integer>[] NoPorS;
    ArrayList<Integer> free_nodes;
    
    
    //tommaso: dati veramente immutabili!!! 
    double[][] raw_u_ij;
    double[] raw_b_h;
}
