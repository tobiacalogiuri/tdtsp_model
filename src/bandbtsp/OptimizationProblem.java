/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bandbtsp;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author user
 */
public interface OptimizationProblem {
    public double apply_tp_to_pathTEST(ArrayList<Integer> sol, double initial_time, TDTSP.type_jam jam);
    public double apply_tp_to_path(ArrayList<Integer> sol, double initial_time, TDTSP.type_jam jam);
    public double apply_tp_to_path_Initial(ArrayList<Integer> sol, double initial_time, TDTSP.type_jam jam);
    public OptimizationProblem getRelaxation(double Ub, boolean first);
    public int getProblemSize();
    public int getProgress();
    public double getLBp();
    public double getValue();
    public double getValue_();
    public double getValue__();
    public double getValuePartial();
    public OptimizationProblemSolution getSolution();
    public boolean isValid(OptimizationProblemSolution ops);
    public OptimizationProblem[] branch(double z, double Ub);
    public void performUpperBounding(double upperbound);
    public void readInstance(File instance, File instance2) throws IOException;
    public void readInstanceTW(File instance, File instance2) throws IOException;
    public boolean writeFileDat() throws IOException;
    public double getInitialUB();
    public Data getData();
    public void prova();
    public long getIotime();
}
