/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bandbtsp;

import cplex.CPLEXSolutionBuilder;
import cplex.SolverConfig;
import cplex.SolverStatus;
import cplex.TSPSolutionBuilder;
import ilog.concert.IloException;
import ilog.concert.IloIntMap;
import ilog.concert.IloNumVarMap;
import ilog.opl.IloCustomOplDataSource;
import ilog.opl.IloOplDataHandler;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class TDTSP implements OptimizationProblem {

    private final static String WDIR = System.getProperty("user.dir") + "/";

    //numero nodi escluso depot iniziale e finale
    private int n = 0;
    //Indici in matrice originale
    public int[] position;
    public int[] rposition;
    //Matrice distanze
    private double[][] costs;
    //Current solution
    private ArrayList<Integer> csolution;
    //ATSP heuristic solution
    private int[] hsolution;
    //FO ATSP
    private double objective;
    //Dati immutabili
    public Data data;
    //Tabulist per evitare configurazioni di costo già visitate
    private ArrayList<Integer> tabuList;

    private double Ub;

    private double ct;
    private File f;
    private double fopartial = 0;
    private double fo_partial = 0;
    private double fo = 0;
    private double fo_ = 0;
    private double fo__ = 0;
    private OptimizationProblemSolution ATSPsolution;
    private double[] ones;
    //private double[][] v_ij;
    //private double[][] u_ij;
    private double[] b_h;
    private double lbp;
    private int max_tp;
    private int min_tp;
    private ArrayList<Integer> partialSolution;
    private double[] new_uij;
    private double[][] uijperbh;
    private long iotime = 0L;
    private double[][] tW_st;

    public enum type_jam {
        z, z_, z__
    }

    public TDTSP(int n, double[][] costs, int[] position, int[] rposition, OptimizationProblemSolution ATSPsolution, ArrayList<Integer> partialSolution, ArrayList<Integer> tabuList, Data data, double Ub, double lbp, double fop, double[] new_uij) {
        this.position = position;
        this.rposition = rposition;
        this.n = n;
        this.costs = costs;
        this.ATSPsolution = ATSPsolution;
        this.csolution = partialSolution;
        this.tabuList = new ArrayList<Integer>();
        this.tabuList.addAll(tabuList);
        this.data = data;
        this.Ub = Ub;
        this.lbp = lbp;
        this.fopartial = fop;
        //u_ij = new double[data.c][data.t];
        // = new double[data.c][data.t];
        b_h = new double[data.t];
        this.new_uij = Arrays.copyOf(new_uij, new_uij.length);
        uijperbh = new double[data.c][data.t];
    }

    public TDTSP() {
        this.csolution = new ArrayList<Integer>(1);
        this.csolution.add(0);
    }

    public TDTSP(File f) {
        f = this.f;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public double getLbp() {
        return lbp;
    }

    @Override
    public long getIotime() {
        return iotime;
    }

    @Override
    public OptimizationProblem getRelaxation(double Ub, boolean first) {
        try {
            this.Ub = Ub;
            //Esegue ATSP e scrive la soluzione
            fo = apply_tp_to_path(csolution, 0, type_jam.z);
            if(first) {
                data.raw_u_ij = new double[data.c][data.t];
                for(int i = 0; i < data.c; i++)
                    data.raw_u_ij[i] = Arrays.copyOf(data.original_u_ij[i], data.t);
            }
            if (first && Main.PREP) {
               calculate(fo);
            } else {
               calculate_noModel_old(fo);
            }
            if(first) {
                data.raw_b_h = Arrays.copyOf(b_h, b_h.length);
            }
            boolean result = true;

            result = writeFileDat2();
            //writeFileDat();
            /*writeFileDat();
            if(!first)
            writeFileDat1();*/

            if (result == false) {
                return this;
            }
            if (csolution == null || partialSolution == null) {
                return this;
            }
            if (partialSolution.get(0) == data.n + 1 && partialSolution.size() > 1) {
                this.fo__ = Double.MAX_VALUE;
                return this;
            }
            partialSolution.add(0, csolution.get(csolution.size() - 1));
            try {
                fo_ = apply_tp_to_path(partialSolution, fo, type_jam.z_);
            } catch (Exception e) {
                fo_ = Ub + 1;
            }
            try {
                fo = apply_tp_to_path(partialSolution, fo, type_jam.z);
            } catch (Exception e) {
                fo = Ub + 1;
            }
            //partialSolution.stream().forEach(i -> System.out.print(i + ", "));
            //System.out.println();
            if(first) {
                /*System.out.println("z1__ = "+apply_tp_to_path(new ArrayList<>(Arrays.asList(0, 8, 11, 12, 15, 13, 14, 9, 6, 10, 7, 3, 1, 2, 4, 5, 16)), 0, type_jam.z__));                
                System.out.println("z1_ = "+apply_tp_to_path(new ArrayList<>(Arrays.asList(0, 8, 11, 12, 15, 13, 14, 9, 6, 10, 7, 3, 1, 2, 4, 5, 16)), 0, type_jam.z_));                
                System.out.println("z1 = "+apply_tp_to_path(new ArrayList<>(Arrays.asList(0, 8, 11, 12, 15, 13, 14, 9, 6, 10, 7, 3, 1, 2, 4, 5, 16)), 0, type_jam.z));                
                */
                System.out.println("z__ = "+apply_tp_to_path(partialSolution, 0, type_jam.z__));       
                System.out.println("z_ = "+apply_tp_to_path(partialSolution, 0, type_jam.z_));  
                System.out.println("z = "+apply_tp_to_path(partialSolution, 0, type_jam.z));     
            }
            //System.exit(0);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return this;
    }

    public double apply_tp(double t_0, double arc_cost, double[] traffic, double[][] times) {
        double t = t_0;
        double cost = arc_cost;
        //Time period di appartenenza di t_0
        int k = 0;
        for (int i = 0; i < times.length; i++) {
            if ((t >= times[i][0]) && (t < times[i][1])) {
                k = i;
                break;
            }
        }
        double t1 = t + cost / traffic[k];
        while (t1 > times[k][1]) {
            cost = cost - traffic[k] * (times[k][1] - t);
            t = times[k][1];
            try {
                t1 = t + cost / traffic[k + 1];
            } catch (Exception e) {
                t1 = t;
            }
            k++;
        }
        return t1 - t_0;
    }

    public double apply_tp2(double t_0, double arc_cost, double[] traffic, double[][] times) {
        double t = t_0;
        double cost = arc_cost;
        //Time period di appartenenza di t_0
        int k = 0;
        for (int i = 0; i < times.length; i++) {
            if ((t >= times[i][0]) && (t < times[i][1])) {
                k = i;
                break;
            }
        }
        double t1 = t + cost / traffic[k];
        while (t1 > times[k][1]) {
            cost = cost - traffic[k] * (times[k][1] - t);
            t = times[k][1];
            t1 = t + cost / traffic[k + 1];
            k++;
        }
        return t1;
    }

//    public double apply_tp_to_path(OptimizationProblemSolution sol, double initial_time, type_jam jam){
//        double total = initial_time;
//        ArrayList<Integer> solution = sol.getSol();
//        int prev = 0;
//        int next;
//        for(int i=0; i< solution.size(); i++){
//            next = solution.get(prev)-1;
//            if(next<0) break;
//            if(jam.equals(type_jam.z_))
//                total += apply_tp(total, costs[prev][next], data.speeds[data.clusters[prev][next]-1], data.tp);
//            else if(jam.equals(type_jam.z)){
//                double[] speeds_delta = new double[data.t];
//                for(int j=0; j<data.t;j++){
//                    speeds_delta[j] = data.deltas[data.clusters[prev][next]-1][j]*data.speeds[data.clusters[prev][next]-1][j];
//                }
//                total += apply_tp(total, data.original_costs[prev][next], speeds_delta, data.tp);
//            }
//            else
//                total += apply_tp(total, data.original_costs[prev][next], ones, data.tp);
//            prev = next;
//        }
//        return total;
//    }
//    public void calculate_noModel(double initialTime){
//        
//        //double[][] uijperbhperdelta = new double[data.c][data.t];
//        
//        //Aggiorno v_ij u_ij e b_h
//            
//            max_tp=1;
//            min_tp=-1;
//            
//            for(int i=0; i<data.t; i++){
//                if(data.tp[i][1]<Ub) max_tp++;
//                if(data.tp[i][0]<=initialTime) min_tp++;
//            }
//            //System.out.println(""+getValuePartial());
//            
//            for(int i=0; i<data.c; i++){
//                new_uij[i] = 0;
//                for(int j=min_tp; j<max_tp; j++){
//                    if(data.original_v_ij[i][j]>new_uij[i])  new_uij[i]= data.original_v_ij[i][j];
//                }
//            }
//            
//            for(int i=0; i<max_tp; i++){
//                b_h[i] = 0;
//                for(int j=0; j<data.c; j++){
//                    if((data.original_v_ij[j][i]/new_uij[j])>b_h[i])  b_h[i]= (data.original_v_ij[j][i]/new_uij[j]);
//                }
//            }
//            
//            for(int i=0; i<data.c; i++){
//                for(int j=0; j<max_tp; j++){
//                    uijperbh[i][j] = new_uij[i]*b_h[j];
//                }
//            }        
//    }
    public void calculate(double initialTime) throws FileNotFoundException, IOException {

        //double[][] uijperbhperdelta = new double[data.c][data.t];
        //Aggiorno v_ij u_ij e b_h     
        max_tp = 1;
        min_tp = -1;

        for (int i = 0; i < data.t; i++) {
            if (data.tp[i][1] < Ub) {
                max_tp++;
            }
            if (data.tp[i][0] <= initialTime) {
                min_tp++;
            }
        }
        //System.out.println(""+getValuePartial());
        int intervals = (max_tp - min_tp);
        double[][] delta = new double[data.c][data.t];
        double[] v_max;
        /*= new double[data.c];
            
            //Calcola velocità massima per ognio cluster
            for(int i=0; i<data.c; i++){
                v_max[i] = 0.0;
                for(int j=min_tp; j<max_tp; j++){
                    if(data.original_v_ij[i][j]>v_max[i])  v_max[i]= data.original_v_ij[i][j];
                }
            }
         */
        //Valido solo per le istanze nostre
        v_max = Arrays.copyOf(new_uij, new_uij.length);
        //Arrays.fill(v_max, 1.0);
        //Calcolo i b_h
        for (int i = 0; i < max_tp; i++) {
            b_h[i] = 0;
            for (int j = 0; j < data.c; j++) {
                if ((data.original_v_ij[j][i] / v_max[j]) > b_h[i]) {
                    b_h[i] = (data.original_v_ij[j][i] / v_max[j]);
                }
            }
        }

        //calcolo i delta
        for (int i = 0; i < data.c; i++) {
            for (int j = 0; j < max_tp; j++) {
                delta[i][j] = BigDecimal.valueOf(data.original_v_ij[i][j] / (v_max[i] * b_h[j])).setScale(6, RoundingMode.CEILING).doubleValue();
            }
        }

        double[] u;

        double u_max = 0.0;
        double c;
        for (int j = min_tp; j < max_tp; j++) {
            u = Arrays.copyOf(v_max, v_max.length);
            c = 0.0;
            double[] delta_temp = new double[data.c];
            for (int i = 0; i < data.c; i++) {
                delta_temp[i] = delta[i][j];
            }
            //Ordino delta e salvo indici
            double temp;

            for (int i = 0; i < data.c; i++) {
                for (int k = i + 1; k < data.c; k++) {
                    if (delta_temp[i] < delta_temp[k]) {
                        temp = delta_temp[i];
                        delta_temp[i] = delta_temp[k];
                        delta_temp[k] = temp;
                        temp = u[i];
                        u[i] = u[k];
                        u[k] = temp;
                    }
                }
            }

            for (int i = 0; i < data.c - 1; i++) {
                c = delta_temp[i + 1];
                for (int k = 0; k < i + 1; k++) {
                    u[k] = u[k] / c;
                }
                for (int k = i + 1; k < data.c; k++) {
                    delta_temp[k] = delta_temp[k] / c;
                }
            }
            for (int i = 0; i < data.c; i++) {
                if (u[i] > u_max) {
                    u_max = u[i];
                }
            }
        }
        u_max = BigDecimal.valueOf(1 / u_max).setScale(6, RoundingMode.CEILING).doubleValue();

        double[] length = new double[data.c];
        Arrays.fill(length, 0.0);
        double total_length = 0.0;
        //data.n = data.clusters.length;

        for (int i = 0; i < data.clusters.length; i++) {
            for (int j = 0; j < data.clusters.length; j++) {
                if (data.clusters[i][j] > 0) {
                    length[data.clusters[i][j] - 1] += 1;
                }
                //length[data.clusters[i][j]-1] += data.original_costs[i][j];
            }
        }
        for (int i = 0; i < data.c; i++) {
            total_length += length[i];
        }

        CPLEXSolutionBuilder cplexBuilder = new CPLEXSolutionBuilder();
        //SolverConfig config = new SolverConfig("model/CalcoloUij.mod", "model/CalcoloUij.dat");
        SolverConfig config = new SolverConfig("model/CalcoloUij.mod");
        double[] u_primo = new double[data.c];
        double tl = total_length;
        double u_m = u_max;
        config.addCustomDataSource(new IloCustomOplDataSource(cplexBuilder.getOplF()) {
            @Override
            public void customRead() {
                IloOplDataHandler handler = getDataHandler();
                handler.startElement("cluster");
                handler.addIntItem(data.c);
                handler.endElement();
                handler.startElement("hTime");
                handler.addIntItem(intervals);
                handler.endElement();
                handler.startElement("v");
                handler.startArray();
                for (int i = 0; i < data.c; i++) {
                    handler.startArray();
                    for (int j = min_tp; j < max_tp; j++) {
                        handler.addNumItem(data.original_v_ij[i][j]);
                    }
                    handler.endArray();
                }
                handler.endArray();
                handler.endElement();
                handler.startElement("v_max");
                handler.startArray();
                for (int i = 0; i < data.c; i++) {
                    handler.addNumItem(v_max[i]);
                }
                handler.endArray();
                handler.endElement();
                handler.startElement("length");
                handler.startArray();
                for (int i = 0; i < data.c; i++) {
                    handler.addNumItem(length[i]);
                }
                handler.endArray();
                handler.endElement();
                handler.startElement("total_length");
                handler.addNumItem(tl);
                handler.endElement();
                handler.startElement("u_max");
                handler.addNumItem(u_m);
                handler.endElement();
            }
        }
        );

        double fo1 = 0.0;
        for (int i = 0; i < data.c; i++) {
            for (int j = min_tp; j < max_tp; j++) {
                fo1 += length[i] * delta[i][j];
            }
        }
        System.out.println("fo1 = " + fo1 / total_length / intervals);

        config.addCallback((opl) -> {
            System.out.println("fo2 = " + opl.getElement("obj2").asNum() / intervals);
            IloNumVarMap x = opl.getElement("u").asNumVarMap();
            //IloNumVarMap y = opl.getElement("b_primo").asNumVarMap();
            try {
                int doublearc = 0;
                for (int i = 0; i < data.c; i++) {
                    u_primo[i] = opl.getCplex().getValue(x.get(i + 1));
                }
                /*
                for(int i = min_tp; i < max_tp; i++) {
                    b_h[i] = 1./opl.getCplex().getValue(y.get(i - min_tp + 1));
                    //System.err.print(b_h[i] + " ");
                }*/
                
            } catch (IloException ex) {
                ex.printStackTrace();
            }
        });
        SolverStatus status = cplexBuilder.buildSolution(config);
        System.out.println("sum_y = " + status.getObjValue());

        for (int i = 0; i < data.c; i++) {
            new_uij[i] = u_primo[i];
            Arrays.fill(data.original_u_ij[i], new_uij[i]);
        }

        for (int i = 0; i < max_tp; i++) {
            b_h[i] = 0;
            for (int j = 0; j < data.c; j++) {
                if ((data.original_v_ij[j][i] / new_uij[j]) > b_h[i]) {
                    b_h[i] = (data.original_v_ij[j][i] / new_uij[j]);
                }
            }
        }

        for (int i = 0; i < data.c; i++) {
            for (int j = 0; j < max_tp; j++) {
                uijperbh[i][j] = new_uij[i] * b_h[j];
            }
        }
       
    }

    public void calculate_noModel(double initialTime) {

        max_tp = 1;
        min_tp = -1;

        double[] k_ij = new double[data.c];
        double k_max = 0;

        int[] c_new = new int[data.c];
        for (int k = 0; k < data.c; ++k) {
            c_new[k] = 0;
        }

        //valido nell'ipotesi che gli archi
        //uscenti da uno stesso nodo appartengano
        //allo stesso cluster
        int sizeCsolution = csolution.size();
        int lastNode = csolution.get(sizeCsolution - 1);
        int sum = 0;

        for (int y = 0; y <= data.n; y++) {
            if (!csolution.contains(y) || lastNode == y) {
                c_new[data.clusters[y][y + 1] - 1] = 1;
            }

        }

        for (int i = 0; i < data.t; i++) {
            if (data.tp[i][1] < Ub) {
                max_tp++;
            }
            if (data.tp[i][0] <= initialTime) {
                min_tp++;
            }
        }
        //versione aggiornamento Anna

//         for (int i = 0; i < data.c; i++) {
//            //new_uij[i] = 0;
//            k_ij[i] = 0;
//            if(c_new[i] == 1)
//            {
//                for (int j = 0; j < max_tp; j++) {
//                    if (data.original_v_ij[i][j] > new_uij[i]) {
//                        new_uij[i] = data.original_v_ij[i][j];
//                    }
//                }
//                double temp = new_uij[i]/data.original_u_ij[i][0];
//                if(temp > k_ij[i]){
//                    k_ij[i] = temp; 
//                }
//                if(k_ij[i]>k_max)
//                    k_max = k_ij[i];
//            }
//        }
//        for (int i = 0; i < data.c; i++) {
//            if(c_new[i] == 1)
//               new_uij[i] = k_max*data.original_u_ij[i][0];
//        }
        for (int i = 0; i < max_tp; i++) {
            b_h[i] = 0;
            for (int j = 0; j < data.c; j++) {
                if (c_new[j] == 1) {
                    if ((data.original_v_ij[j][i] / new_uij[j]) > b_h[i]) {
                        b_h[i] = (data.original_v_ij[j][i] / new_uij[j]);
                    }
                }
            }
        }

        for (int i = 0; i < data.c; i++) {
            if (c_new[i] == 1) {
                for (int j = 0; j < max_tp; j++) {
                    uijperbh[i][j] = new_uij[i] * b_h[j];
                }
            }
        }

    }

    public void calculate_noModel_old(double initialTime) {

        max_tp = 1;
        min_tp = -1;

        double[] k_ij = new double[data.c];
        double k_max = 0;

        int[] c_new = new int[data.c];
        for (int k = 0; k < data.c; ++k) {
            c_new[k] = 0;
        }

        //valido nell'ipotesi che gli archi
        //uscenti da uno stesso nodo appartengano
        //allo stesso cluster
        int sizeCsolution = csolution.size();
        int lastNode = csolution.get(sizeCsolution - 1);
        int sum = 0;

        for (int y = 0; y <= data.n; y++) {
            if (!csolution.contains(y) || lastNode == y) {
                for(int w =0; w <= data.n; w++)
                    if(y != w)
                        c_new[data.clusters[y][w] - 1] = 1;
            }

        }
        if(Main.FIRST_SOL) { /// durante la ricerca della prima soluzione non rimuove cluster!
            for (int k = 0; k < data.c; ++k) {
                c_new[k] = 1;
            }
        }
        for (int i = 0; i < data.t; i++) {
            if (data.tp[i][1] < Ub) {
                max_tp++;
            }
            if (data.tp[i][0] <= initialTime) {
                min_tp++;
            }
        }
        //versione aggiornamento Anna

        for (int i = 0; i < data.c; i++) {
            new_uij[i] = 0;
            k_ij[i] = 0;
            if (c_new[i] == 1) {
                for (int j = 0; j < max_tp; j++) {
                    if (data.original_v_ij[i][j] > new_uij[i]) {
                        new_uij[i] = data.original_v_ij[i][j];
                    }
                }

                double temp = new_uij[i] / data.original_u_ij[i][0];
                if (temp > k_ij[i]) {
                    k_ij[i] = temp;
                }
                if (k_ij[i] > k_max) {
                    k_max = k_ij[i];
                }
            }

        }

        for (int i = 0; i < data.c; i++) {
            if (c_new[i] == 1) {
                new_uij[i] = k_max * data.original_u_ij[i][0];
            }
        }

        for (int i = 0; i < max_tp; i++) {
            b_h[i] = 0;
            for (int j = 0; j < data.c; j++) {
                if (c_new[j] == 1) {
                    if ((data.original_v_ij[j][i] / new_uij[j]) > b_h[i]) {
                        b_h[i] = (data.original_v_ij[j][i] / new_uij[j]);
                    }
                }
            }
        }

        for (int i = 0; i < data.c; i++) {
            if (c_new[i] == 1) {
                for (int j = 0; j < max_tp; j++) {
                    uijperbh[i][j] = new_uij[i] * b_h[j];
                }
            }
        }

    }

    @Override
    public double apply_tp_to_path(ArrayList<Integer> sol, double initial_time, type_jam jam) {
        double total = initial_time;
        ArrayList<Integer> solution = sol;

        for (int i = 0; i < solution.size() - 1; i++) {
            if (jam.equals(type_jam.z_)) {
                total += apply_tp(total, data.original_costs[solution.get(i)][solution.get(i + 1)], uijperbh[data.clusters[solution.get(i)][solution.get(i + 1)] - 1], data.tp);
            } else if (jam.equals(type_jam.z)) {
                total += apply_tp(total, data.original_costs[solution.get(i)][solution.get(i + 1)], data.original_v_ij[data.clusters[solution.get(i)][solution.get(i + 1)] - 1], data.tp);
                //System.out.print(total+"-");
//total += apply_tp(total, data.original_costs[solution.get(i)][solution.get(i+1)], uijperbhperdelta[data.clusters[solution.get(i)][solution.get(i+1)]-1], data.tp);
            } else {
                total += apply_tp(total, data.original_costs[solution.get(i)][solution.get(i + 1)], data.original_u_ij[data.clusters[solution.get(i)][solution.get(i + 1)] - 1], data.tp);
            }
        }
        // System.out.println("");
        return total;
    }

    @Override
    public double apply_tp_to_pathTEST(ArrayList<Integer> sol, double initial_time, type_jam jam) {
        double total = initial_time;
        ArrayList<Integer> solution = sol;

        for (int i = 0; i < solution.size() - 1; i++) {
            if (jam.equals(type_jam.z_)) {
                total += apply_tp(total, data.original_costs[solution.get(i)][solution.get(i + 1)], uijperbh[data.clusters[solution.get(i)][solution.get(i + 1)] - 1], data.tp);
            } else if (jam.equals(type_jam.z)) {
                total += apply_tp(total, data.original_costs[solution.get(i)][solution.get(i + 1)], data.original_v_ij[data.clusters[solution.get(i)][solution.get(i + 1)] - 1], data.tp);
                System.out.print(total + "-");
//total += apply_tp(total, data.original_costs[solution.get(i)][solution.get(i+1)], uijperbhperdelta[data.clusters[solution.get(i)][solution.get(i+1)]-1], data.tp);
            } else {
                total += apply_tp(total, data.original_costs[solution.get(i)][solution.get(i + 1)], data.original_u_ij[data.clusters[solution.get(i)][solution.get(i + 1)] - 1], data.tp);
            }
        }
        //System.out.print(total + " ");
        return total;
    }

    public double apply_tp_to_path(ArrayList<Integer> sol, int offset, double initial_time, type_jam jam) {
        double total = initial_time;
        ArrayList<Integer> solution = sol;

        for (int i = offset; i < solution.size() - 1; i++) {
            if (jam.equals(type_jam.z_)) {
                total += apply_tp(total, data.original_costs[solution.get(i)][solution.get(i + 1)], uijperbh[data.clusters[solution.get(i)][solution.get(i + 1)] - 1], data.tp);
            } else if (jam.equals(type_jam.z)) {
                total += apply_tp(total, data.original_costs[solution.get(i)][solution.get(i + 1)], data.original_v_ij[data.clusters[solution.get(i)][solution.get(i + 1)] - 1], data.tp);
            } else {
                total += apply_tp(total, data.original_costs[solution.get(i)][solution.get(i + 1)], data.original_u_ij[data.clusters[solution.get(i)][solution.get(i + 1)] - 1], data.tp);
            }
        }
        return total;
    }

    @Override
    public double apply_tp_to_path_Initial(ArrayList<Integer> sol, double initial_time, type_jam jam) {
        double total = initial_time;
        ArrayList<Integer> solution = sol;

        for (int i = 0; i < solution.size() - 1; i++) {
            if (jam.equals(type_jam.z_)) {
                total += apply_tp(total, data.original_costs[solution.get(i)][solution.get(i + 1)], data.speeds[data.clusters[solution.get(i)][solution.get(i + 1)] - 1], data.tp);
            } else if (jam.equals(type_jam.z)) {
                total += apply_tp(total, data.original_costs[solution.get(i)][solution.get(i + 1)], data.original_v_ij[data.clusters[solution.get(i)][solution.get(i + 1)] - 1], data.tp);
//total += apply_tp(total, data.original_costs[solution.get(i)][solution.get(i+1)], uijperbhperdelta[data.clusters[solution.get(i)][solution.get(i+1)]-1], data.tp);
            } else {
                total += apply_tp(total, data.original_costs[solution.get(i)][solution.get(i + 1)], data.original_u_ij[data.clusters[solution.get(i)][solution.get(i + 1)] - 1], data.tp);
            }
        }
        System.out.println("");
        return total;
    }

    @Override
    public int getProblemSize() {
        return n;
    }

    @Override
    public double getValue() {
        return fo;
    }

    @Override
    public double getValuePartial() {
        return fopartial;
    }

    @Override
    public double getValue_() {
        return fo_;
    }

    @Override
    public double getValue__() {
        return fo__;
    }

    @Override
    public OptimizationProblemSolution getSolution() {
        return ATSPsolution;
    }

    @Override
    public int getProgress() {
        return csolution.size();
    }

    @Override
    public boolean isValid(OptimizationProblemSolution ops) {
        ArrayList<Integer> sol = ops.getSol();
        boolean cond = true;
        if (ops.getLb() != fo) {
            cond = false;
        }

        return cond;
    }

    @Override
    public OptimizationProblem[] branch(double z, double Ub) {
        OptimizationProblemSolution current = getSolution();
        OptimizationProblem[] problem_list = new TDTSP[current.sol.size()];
        //Ordina per ordine di visita dei nodi
        ArrayList<Integer> newSol = current.getSol();
        ArrayList<Integer> tList = new ArrayList<>(tabuList);
        int[] position2 = new int[current.sol.size()];
        int[] rposition2 = new int[current.sol.size()];
        for (int i = 0; i < newSol.size(); i++) {
            position2[i] = newSol.get(i);
            //rposition2[newSol.get(i)] = i;
        }
        int i;
        int counter = 0;
        //int num_equals = 0;
        //Branch m-ario
        double scost = apply_tp_to_path(newSol, 0, type_jam.z__);
        
        /*double scostRAW = 0;
        for (int it = 0; it < newSol.size() - 1; it++) {
            scostRAW += apply_tp(scostRAW, data.original_costs[newSol.get(it)][newSol.get(it + 1)], data.raw_u_ij[data.clusters[newSol.get(it)][newSol.get(it + 1)] - 1], data.tp);
        }*/
        
        for (i = csolution.size(); i < newSol.size(); i++) {
            double[][] new_costs = new double[newSol.size() - i + 1][newSol.size() - i + 1];
            //Vettore dei già fissati
            ArrayList<Integer> na = new ArrayList<>(csolution.size() + i - 1);
//            ArrayList<Integer> na2 = new ArrayList<>(csolution.size() + i - 1);
            //Vettore dei rimanenti
            ArrayList<Integer> index_list = new ArrayList<>(newSol);
            na.addAll(csolution);
//            na2.add(csolution.get(csolution.size() -1));
            for (int ii = csolution.size(); ii < i; ii++) {
                na.add(newSol.get(ii));
//                na2.add(newSol.get(ii));
            }
            //ArrayList<Integer> sna = convertSolutionToSuccessorList(na);
            OptimizationProblemSolution OPsol = new OptimizationProblemSolution(na);

            double p1 = 0.0;
            double fop = 0.0;
            fop = apply_tp_to_path(na, 0, type_jam.z);
            //calculate(fop);
            double lbp2 = 0;
            double scostpartial = apply_tp_to_path(na, 0, type_jam.z__);
            /*double scostpartialRAW = 0;//apply_tp_to_path(na, 0, type_jam.z__);
            for (int it = 0; it < newSol.size() - 1; it++) {
                scostpartialRAW += apply_tp(scostpartialRAW, data.original_costs[newSol.get(it)][newSol.get(it + 1)], data.raw_u_ij[data.clusters[newSol.get(it)][newSol.get(it + 1)] - 1], data.tp);
            }*/
            try {
                lbp2 = apply_tp2(fop, scost - scostpartial, b_h, data.tp);
            } catch (Exception e) {
                lbp2 = Ub + 1;
            }
            
            /*double lbp2RAW = 0;
            try {
                lbp2RAW = apply_tp2(fop, scostRAW - scostpartialRAW, data.raw_b_h, data.tp);
            } catch (Exception e) {
                lbp2RAW = Ub + 1;
            }
            lbp2 = Math.max(lbp2, lbp2RAW);*/
            OPsol.setLb(lbp2);

//            double fop_lb  = apply_tp_to_path(csolution, 0, type_jam.z); //aa:calcolo costo sequenza parziale 
//            fop_lb  = apply_tp_to_path(na2,fop_lb , type_jam.z_); //aacon tw e delta = 1;
//            if(fop_lb==fop && i!= csolution.size()){
//                boolean uno = true;
//                for (int j = csolution.size()+1; j < i-1; j++) {
//                    if(uno){
//                        uno = false;
//                        counter--;
//                    }
////                    System.out.println("Cancello - "+j+" - "+index_list.toString());
//                    problem_list[j]=null;
//                }
//            }
            if (lbp2 > Ub && i > csolution.size()) {
                break;
            }

//            double z_na = apply_tp_to_path(na, 0, type_jam.z_);
//            if(z_na==fop && fop!=0)
//                continue;
//            
            counter++;

            for (int j = i - 1; j < index_list.size(); j++) {
                for (int k = i - 1; k < index_list.size(); k++) {
                    new_costs[j - i + 1][k - i + 1] = data.original_costs[position2[j]][position2[k]];
                }
            }

            if (tList.size() == index_list.size() - csolution.size()) {
                counter--;
                continue;
            }
            if (i == csolution.size()) {
                tList.add(position2[i]);
                for (int k = i - 1; k < index_list.size(); k++) {
                    if (tList.contains(position2[k])) {
                        new_costs[0][k - i + 1] = 1000;
                    }
                }
            } else if (index_list.size() - na.size() > 2) {
                new_costs[0][1] = 1000;
                tList.clear();
                tList.add(position2[i]);
            }

            problem_list[i] = new TDTSP(problem_list.length - 2 - i + 1, new_costs, position2, rposition2, OPsol, na, tList, getData(), Ub, lbp2, fop, new_uij);
            tList.clear();
        }
//        counter = 0;
//        for(i = 0; i< problem_list.length; i++){
//            if(problem_list[i]!=null) counter++;
//        }
        //Elimino i null
        OptimizationProblem[] problem_list2 = null;
        try {
            problem_list2 = new TDTSP[counter];
        } catch (Exception e) {
            problem_list2 = new TDTSP[0];
        }
        int nulls = 0;
        for (i = 0; i < problem_list.length; i++) {
            if (problem_list[i] != null) {
                problem_list2[nulls] = problem_list[i];
                nulls++;
            }
        }
        return problem_list2;
    }

    public ArrayList<Integer> convertSolution(OptimizationProblemSolution sol) {
        ArrayList<Integer> newSol = new ArrayList<Integer>(sol.getSol().size());
        int prev = 0;
        int next;
        for (int i = 0; i < sol.getSol().size(); i++) {
            next = sol.getSol().get(prev) - 1;
            newSol.add(prev);
            prev = next;
        }
        return newSol;
    }

    public ArrayList<Integer> convertSolution(ArrayList<Integer> sol) {
        ArrayList<Integer> newSol = new ArrayList<Integer>(sol.size());
        int prev = 0;
        int next;
        for (int i = 0; i < sol.size(); i++) {
            next = sol.get(prev) - 1;
            newSol.add(prev);
            prev = next;
        }
        return newSol;
    }

    public ArrayList<Integer> convertSolutionToSuccessorList(ArrayList<Integer> sol) {
        ArrayList<Integer> newSol = new ArrayList<Integer>(n + 2);
        for (int i = 0; i < n + 2; i++) {
            newSol.add(0);
        }
        for (int i = 0; i < sol.size() - 1; i++) {
            newSol.set(sol.get(i), sol.get(i + 1) + 1);
        }
//        if((sol.size()-1)>0){
//            newSol.set(sol.get(sol.size()-1), 0);
//        }
        return newSol;
    }

    @Override
    public void performUpperBounding(double upperbound) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void readInstanceTW(File instance, File instance2) throws IOException {
        data = new Data();
        f = instance;
        int primaC = 0;
        int[] cluster_count = {};
        double[] cluster_delta = {};

        FileInputStream fis = new FileInputStream(instance);
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);
        String[] array;

        String line = br.readLine();

        boolean continua = true;

        //aa: node number
        n = Integer.parseInt(line) - 1;
        data.n = n;
        //aa: original cost matrix
        data.original_costs = new double[n + 2][n + 2];
        data.forbidden_arcs = new int[n + 2][n + 2];
        for (int i = 0; i < (n + 1); i++) {
            line = br.readLine();
            line = line.trim();
            array = line.split(" ");
            for (int j = 0; j < (n + 1); j++) {
                data.original_costs[i][j] = Double.parseDouble(array[j].trim());
            }
        }
        for (int i = 1; i < n + 1; i++) {
            data.original_costs[i][n + 1] = data.original_costs[i][0];
        }

        //aa: time windows
        data.tW_td = new double[n + 2][2];
        data.max_arrival_time = 0;
        for (int i = 0; i < n + 1; ++i) {
            line = br.readLine();
            line = line.trim();
            array = line.split("\t");
            data.tW_td[i][0] = Double.parseDouble(array[0].trim());
            data.tW_td[i][1] = Double.parseDouble(array[1].trim());
            if (data.tW_td[i][0] > data.max_arrival_time) {
                data.max_arrival_time = data.tW_td[i][0];
                data.max_arrival_time_node = i;
            }
        }
        data.tW_td[n + 1][0] = data.tW_td[0][0];
        data.tW_td[n + 1][1] = data.tW_td[0][1];

        while (continua) {
            line = br.readLine();
            if (line == null) {
                break;
            }
            line = line.trim();

            array = line.split(":");
            switch (array[0].trim()) {

                case "C":
                    int max = 0;
                    data.clusters = new int[n + 2][n + 2];
                    for (int i = 0; i < (n + 2); i++) {
                        line = br.readLine();
                        line = line.trim();
                        array = line.split(" ");
                        for (int j = 0; j < (n + 2); j++) {

                            data.clusters[i][j] = Integer.parseInt(array[j].trim());
                            if (max < data.clusters[i][j]) {
                                max = data.clusters[i][j];
                            }
                        }
                    }
                    data.c = max;
                    break;

                case "Time":
                    ArrayList<double[]> v = new ArrayList();
                    int count = 0;
                    line = br.readLine();
                    line = line.trim();
                    array = line.split("\t");
                    String end = array[0].trim();
                    data.tp = new double[data.t][2];
                    while (!end.equals("Speed:")) {
                        double[] lettura = new double[2];
                        //for (int i = 0; i < data.t; i++) {
                        lettura[0] = Double.parseDouble(array[0].trim());
                        lettura[1] = Double.parseDouble(array[1].trim());
                        v.add(lettura);
                        ++count;
                        line = br.readLine();
                        line = line.trim();
                        array = line.split("\t");
                        end = array[0];

                    }

                    data.t = count;
                    data.tp = new double[v.size()][2];
                    for (int i = 0; i < data.t; i++) {
                        data.tp[i][0] = v.get(i)[0];
                        data.tp[i][1] = v.get(i)[1];
                    }

//                    break;
//                case "Speed":
                    data.speeds = new double[data.c][data.t];
                    for (int i = 0; i < data.c; i++) {
                        line = br.readLine();
                        line = line.trim();
                        array = line.split("\t");
                        for (int j = 0; j < data.t; j++) {
                            data.speeds[i][j] = Double.parseDouble(array[j].trim());
                        }

                    }

                    break;

                case "Start_solution":
                    data.backup_solution = new ArrayList<>(n + 2);
                    line = br.readLine();
                    line = line.trim();
                    array = line.split(" ");
                    for (int i = 0; i < (n + 2); i++) {
                        data.backup_solution.add(Integer.parseInt(array[i].trim()));
                    }
                    continua = false;
                    break;

            }
            if (line.equals("EOF")) {
                break;
            }
        }// Fine lettura istanza1

        fis = new FileInputStream(instance2);
        isr = new InputStreamReader(fis);
        br = new BufferedReader(isr);
        line = "";
        //String[] array;
        data.deltas = new double[data.c][data.t];
        cluster_delta = new double[data.c];
        for (int i = 0; i < data.c; i++) {
            cluster_delta[i] = 1.0;
            line = br.readLine();
            line = line.trim();
            array = line.split("\t");
            for (int j = 0; j < data.t; j++) {
                array[j] = array[j].trim().replace(',', '.');
                data.deltas[i][j] = Double.parseDouble(array[j].trim());
                if (data.deltas[i][j] < cluster_delta[i]) {
                    cluster_delta[i] = data.deltas[i][j];
                }
            }
        }
//        int sum =0;
//        double media = 0.0;
//        cluster_count = new int[data.c];
//        for (int i = 0; i < (n + 2); i++) {
//            for (int j = 0; j < (n + 2); j++) {
//                if(data.clusters[i][j]!=0)
//                    cluster_count[data.clusters[i][j]-1]++;
//            }
//        }
//        for(int i=0; i<data.c; i++){
//            sum +=cluster_count[i];
//            media += cluster_delta[i]*cluster_count[i];
//        }
//        media = media/sum;
//        System.out.print(media+";");
        data.solution = new ArrayList<>(n + 2);

        for (int i = 0; i < (n + 2); i++) {
            data.solution.add(i);
        }
        data.z__value = -1;

        position = new int[n + 2];
        for (int i = 0; i < n + 2; i++) {
            position[i] = i;
        }

        data.original_u_ij = new double[data.c][data.t];
        // = new double[data.c][data.t];
        b_h = new double[data.t];
        double max = 0.0;

        for (int j = 0; j < data.t; j++) {
            if (data.speeds[0][j] > max) {
                max = data.speeds[0][j];
            }
        }
        for (int j = 0; j < data.t; j++) {
            b_h[j] = data.speeds[0][j] / max;
        }
        for (int j = 0; j < data.c; j++) {
            for (int k = 0; k < data.t; k++) {
                data.original_u_ij[j][k] = data.speeds[j][k] / b_h[k];
            }
        }

        data.original_v_ij = new double[data.c][data.t];
        for (int j = 0; j < data.c; j++) {
            for (int k = 0; k < data.t; k++) {
                data.original_v_ij[j][k] = data.deltas[j][k] * data.speeds[j][k];
                //[j][k] = data.deltas[j][k]*data.speeds[j][k];
            }
        }

        tabuList = new ArrayList<Integer>(0);
        tabuList.add(n + 1);
        ones = new double[data.t];
        b_h = new double[data.t];
        new_uij = new double[data.c];
        for(int k = 0; k < data.c; k++)
            new_uij[k] = data.original_u_ij[k][0];
        //Arrays.fill(new_uij, 1.0);
        uijperbh = new double[data.c][data.t];

        Arrays.fill(ones, 1.0);
        costs = new double[n + 2][n + 2];
        tW_st = new double[n + 2][n + 2];
        for (int i = 0; i < n + 2; i++) {
            System.arraycopy(data.original_costs[i], 0, costs[i], 0, n + 2);
        }

        //data.landmarks = new ArrayList<>();
        data.Predecessors = (ArrayList<Integer>[]) new ArrayList[n + 1];
        data.Successors = (ArrayList<Integer>[]) new ArrayList[n + 1];
        data.NoPorS = (ArrayList<Integer>[]) new ArrayList[n + 1];
        data.free_nodes = new ArrayList<>(n + 1);

        for (int ii = 0; ii < n + 1; ii++) {
            data.free_nodes.add(ii);
        }
/*
        for (int k = 0; k < data.free_nodes.size(); k++) {
            data.Predecessors[k] = new ArrayList<>();
            data.Successors[k] = new ArrayList<>();
            data.NoPorS[k] = new ArrayList<>();
            for (int j = 1; j < data.free_nodes.size(); j++) {
                if (k == j) {
                    continue;
                }
                if (data.tW_td[data.free_nodes.get(k)][0] > data.tW_td[data.free_nodes.get(j)][1] + apply_tp(data.tW_td[data.free_nodes.get(j)][1], data.original_costs[data.free_nodes.get(j)][data.free_nodes.get(k)], data.original_v_ij[data.clusters[data.free_nodes.get(j)][data.free_nodes.get(k)] - 1], data.tp)) {
                    data.Predecessors[k].add(data.free_nodes.get(j));
                } else if (data.tW_td[data.free_nodes.get(k)][1] <= data.tW_td[data.free_nodes.get(j)][0]) {
                    if (data.tW_td[data.free_nodes.get(k)][0] + apply_tp(data.tW_td[data.free_nodes.get(k)][1], data.original_costs[data.free_nodes.get(k)][data.free_nodes.get(j)], data.original_v_ij[data.clusters[data.free_nodes.get(k)][data.free_nodes.get(j)] - 1], data.tp) <= data.tW_td[data.free_nodes.get(j)][1]) {
                        data.Successors[k].add(data.free_nodes.get(j));
                    } else {
                        data.NoPorS[k].add(data.free_nodes.get(j));
                    }
                } else {
                    data.NoPorS[k].add(data.free_nodes.get(j));
                }
            }
            //if(data.NoPorS[k].size()==0) data.landmarks.add(data.free_nodes.get(k));
            //System.out.println("Nodo "+ free_nodes.get(k)+" ha #" + Predecessors[k].size()+" predecessori #"+ Successors[k].size()+" successori e #"+ NoPorS[k].size()+" niente");
        }
*/
        //forbiden arcs
    }

    @Override
    public void readInstance(File instance, File instance2) throws IOException {
        data = new Data();
        f = instance;
        int primaC = 0;

        FileInputStream fis = new FileInputStream(instance);
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);
        String line = "";

        boolean continua = true;

        while (continua) {
            line = br.readLine();
            line = line.trim();
            String[] array = line.split(":");
            switch (array[0].trim()) {
                case "N":
                    n = Integer.parseInt(array[1].trim());
                    break;
                case "T":
                    data.t = Integer.parseInt(array[1].trim());
                    break;
                case "C":
                    if (primaC == 0) {
                        data.c = Integer.parseInt(array[1].trim());
                    } else {
                        data.clusters = new int[n + 2][n + 2];
                        for (int i = 0; i < (n + 2); i++) {
                            line = br.readLine();
                            line = line.trim();
                            array = line.split("  ");
                            for (int j = 0; j < (n + 2); j++) {
                                data.clusters[i][j] = Integer.parseInt(array[j].trim());
                            }
                        }
                        continua = false;
                    }
                    primaC++;
                    break;
                case "D":
                    data.original_costs = new double[n + 2][n + 2];
                    for (int i = 0; i < (n + 2); i++) {
                        line = br.readLine();
                        line = line.trim();
                        array = line.split("\t");
                        for (int j = 0; j < (n + 2); j++) {
                            data.original_costs[i][j] = Double.parseDouble(array[j].trim());
                        }
                    }
                    break;
                case "Times":
                    data.tp = new double[data.t][2];
                    for (int i = 0; i < data.t; i++) {
                        line = br.readLine();
                        line = line.trim();
                        array = line.split("\t");
                        data.tp[i][0] = Double.parseDouble(array[0].trim());
                        data.tp[i][1] = Double.parseDouble(array[1].trim());

                    }
                    break;
                case "Speed":
                    data.speeds = new double[data.c][data.t];
                    for (int i = 0; i < data.c; i++) {
                        line = br.readLine();
                        line = line.trim();
                        array = line.split("\t");
                        for (int j = 0; j < data.t; j++) {
                            data.speeds[i][j] = Double.parseDouble(array[j].trim());
                        }
                    }
                    break;
                case "S":
                    data.solution = new ArrayList<>(n + 2);
                    line = br.readLine();
                    line = line.trim();
                    array = line.split(" ");
                    for (int i = 0; i < (n + 2); i++) {
                        data.solution.add(Integer.parseInt(array[i].trim()));
                    }
                    break;
                case "Cost":
                    line = br.readLine();
                    line = line.trim();
                    objective = Double.parseDouble(line);
                    break;
                case "CT":
                    line = br.readLine();
                    line = line.trim();
                    ct = Double.parseDouble(line);
                    break;
            }
            if (line.equals("EOF")) {
                break;
            }
        }// Fine lettura istanza1

        fis = new FileInputStream(instance2);
        isr = new InputStreamReader(fis);
        br = new BufferedReader(isr);
        line = "";
        String[] array;
        data.deltas = new double[data.c][data.t];
        for (int i = 0; i < data.c; i++) {
            line = br.readLine();
            line = line.trim();
            array = line.split("\t");
            for (int j = 0; j < data.t; j++) {
                data.deltas[i][j] = Double.parseDouble(array[j].trim());
            }
        }
        position = new int[n + 2];
        for (int i = 0; i < n + 2; i++) {
            position[i] = i;
        }

        data.original_u_ij = new double[data.c][data.t];
        // = new double[data.c][data.t];
        b_h = new double[data.t];
        double max = 0.0;

        for (int j = 0; j < data.t; j++) {
            if (data.speeds[0][j] > max) {
                max = data.speeds[0][j];
            }
        }
        for (int j = 0; j < data.t; j++) {
            b_h[j] = data.speeds[0][j] / max;
        }
        for (int j = 0; j < data.c; j++) {
            for (int k = 0; k < data.t; k++) {
                data.original_u_ij[j][k] = data.speeds[j][k] / b_h[k];
            }
        }

        data.original_v_ij = new double[data.c][data.t];
        for (int j = 0; j < data.c; j++) {
            for (int k = 0; k < data.t; k++) {
                data.original_v_ij[j][k] = data.deltas[j][k] * data.speeds[j][k];
                //[j][k] = data.deltas[j][k]*data.speeds[j][k];
            }
        }

        tabuList = new ArrayList<Integer>(0);
        ones = new double[data.t];
        b_h = new double[data.t];
        new_uij = new double[data.c];
        uijperbh = new double[data.c][data.t];
        Arrays.fill(ones, 1.0);
        costs = new double[n + 2][n + 2];
        for (int i = 0; i < n + 2; i++) {
            System.arraycopy(data.original_costs[i], 0, costs[i], 0, n + 2);
        }
    }

    @Override
    public boolean writeFileDat() throws IOException {
        //nome del file .dat da creare
        File del = new File("./TSPsolver/Ris.DAT");
        if (!(del.delete())) {
            System.out.println(del.getName() + " is deleted!");
        }

        File out = new File("./TSPsolver/INP.DAT");
        StringBuilder str;
        long time = System.nanoTime();
        try (FileOutputStream fileDat = new FileOutputStream(out);
                PrintStream outputDat = new PrintStream(fileDat)) {

            //comincio a scrivere il file .dat
            //scrivo il parametro n (numero di nodi)
            outputDat.println((n + 2));
            str = new StringBuilder();
            str.append("100000").append(" ");
            for (int j = 1; j < (n + 2); j++) {
                if (costs[0][j] != 1000) {
                    if(Main.PREP && Main.FORK)
                        str.append((int) ((costs[0][j] / new_uij[data.clusters[position[csolution.size() - 1]][position[csolution.size() - 1 + j]] - 1]) * 100)).append(" ");
                    else
                        str.append((int) ((apply_tp(getValuePartial(), costs[0][j], data.original_v_ij[data.clusters[position[csolution.size() - 1]][position[csolution.size() - 1 + j]] - 1], data.tp)) * 100)).append(" ");
                } else {
                    str.append("100000").append(" ");
                }
            }
            str.deleteCharAt(str.length() - 1);
            str.append("\r\n");
            for (int i = 1; i < (n + 2); i++) {
                //for(int i=0; i<(n+2); i++){
                for (int j = 0; j < (n + 2); j++) {
                    if (i == j) {
                        str.append("100000").append(" ");
                    } //                    else if(i==0 && j<n+1)
                    //                        str.append((int)(Integer.MAX_VALUE)).append(" ");
                    else if (i == n + 1 && j > 0) {
                        str.append("100000").append(" ");
                    } else //str.append((int)((costs[i][j])*100)).append(" ");
                    {
                        str.append((int) ((costs[i][j] / new_uij[data.clusters[position[csolution.size() - 1 + i]][position[csolution.size() - 1 + j]] - 1]) * 100)).append(" ");
                    }

                }
                str.deleteCharAt(str.length() - 1);
                str.append("\r\n");
            }
            outputDat.println(str.toString());
            outputDat.close();
            fileDat.close();
        }
        Process TSP = new ProcessBuilder(WDIR + "TSPsolver/atsp_750.exe").directory(new File(WDIR + "TSPsolver")).start();
        //Process TSP = new ProcessBuilder("/utenti/calogiuri/TDTSP/PREPROCESSING/TSPsolver/atsp_750.exe").directory(new File("/utenti/calogiuri/TDTSP/PREPROCESSING/TSPsolver/")).start();
        StreamGobbler errorGobbler = new StreamGobbler(TSP.getErrorStream(), "ERROR");

        // any output?
        StreamGobbler outputGobbler = new StreamGobbler(TSP.getInputStream(), "OUTPUT");

        // kick them off
        errorGobbler.start();
        outputGobbler.start();
        time = System.nanoTime() - time;
        iotime += time;
        try {
            boolean exit = TSP.waitFor(5, TimeUnit.MINUTES);
            if (exit == false) {
                System.out.println("ATSP No solution");
                //System.out.println(str.toString());
                TSP.destroyForcibly();
                return false;
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(TDTSP.class.getName()).log(Level.SEVERE, null, ex);
        }
        //TSP.destroyForcibly();
        partialSolution = new ArrayList<>(n + 2);

        long writetime = System.nanoTime();
        File ris = new File("./TSPsolver/Ris.DAT");
        if (ris.exists()) {
            FileInputStream fis = new FileInputStream(ris);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            String line = "";
            while (true) {
                line = br.readLine();
                if (line == null) {
                    break;
                }
                line = line.trim();
                String[] array = line.split("=|:");
                switch (array[0].trim()) {
                    case "ZSTAR":
                        String res = array[1].split("\\s+")[1];
                        //System.out.println(res);
                        if (res.contains("*") || res.contains("LB0")) {
                            fo__ = Double.MAX_VALUE;
                        } else {
                            fo__ = Integer.parseInt(array[1].split("\\s+")[1]);
                        }
                        break;
                    case "SOL":
                        while (true) {
                            line = br.readLine();
                            if (line == null) {
                                break;
                            }
                            line = line.trim();
                            String[] array2 = line.split("\\s+");
                            //Il primo è già deciso
                            for (int i = 0; i < array2.length; i++) {
                                partialSolution.add(Integer.parseInt(array2[i]));
                            }
                        }
                        break;
                }
            } //end while

            partialSolution = convertSolution(partialSolution);
            partialSolution.remove(0);
       /*     partialSolution.stream().forEach(i -> System.err.print(i+" "));
            System.err.println();*/
            for (int i = 0; i < partialSolution.size(); i++) {
                partialSolution.set(i, position[csolution.size() - 1 + partialSolution.get(i)]);
            }
            ArrayList<Integer> complete = new ArrayList<>(csolution.size() + partialSolution.size());
            complete.addAll(csolution);
            complete.addAll(partialSolution);
            ATSPsolution = new OptimizationProblemSolution(complete, fo);
 //System.err.println(apply_tp_to_path(complete, 0, type_jam.z__));
            fis.close();
            isr.close();
            br.close();
            return true;
        } //end if ris exist
        writetime = System.nanoTime() - writetime;
        iotime += writetime;
//        double costo = 0;
//        for(int i=0; i< sol.size();i++){
//            costo+=costs[i][sol.get(i)-1];
//        }
        return false;
    }

    public boolean writeFileDat1() throws IOException {
        CPLEXSolutionBuilder cplexBuilder = new CPLEXSolutionBuilder();
        SolverConfig config = new SolverConfig("model/mtz.mod");
        config.addCustomDataSource(new IloCustomOplDataSource(cplexBuilder.getOplF()) {
            @Override
            public void customRead() {
                IloOplDataHandler handler = getDataHandler();
                handler.startElement("N");
                handler.addIntItem(n + 2);
                handler.endElement();

                handler.startElement("dist");
                handler.startArray();
                handler.startArray();
                handler.addIntItem(0);
                for (int j = 1; j < (n + 2); j++) {
                    if (costs[0][j] != 1000) {
                        if(Main.PREP && Main.FORK)
                            handler.addIntItem((int) ((costs[0][j] / new_uij[data.clusters[position[csolution.size() - 1 ]][position[csolution.size() - 1 + j]] - 1]) * 100));
                        else
                            handler.addIntItem((int) ((apply_tp(getValuePartial(), costs[0][j], data.original_v_ij[data.clusters[position[csolution.size() - 1]][position[csolution.size() - 1 + j]] - 1], data.tp)) * 100));
                    } else {
                        handler.addIntItem(100000);
                    }
                }
                handler.endArray();
                for (int i = 1; i < (n + 2); i++) {
                    handler.startArray();
                    for (int j = 0; j < (n + 2); j++) {
                        if (i == j) {
                            handler.addIntItem(0);
                        } //                    else if(i==0 && j<n+1)
                        //                        str.append((int)(Integer.MAX_VALUE)).append(" ");
                        else if (i == n + 1 && j > 0) {
                            handler.addIntItem(100000);
                        } else //str.append((int)((costs[i][j])*100)).append(" ");
                        {
                            handler.addIntItem((int) ((costs[i][j] / new_uij[data.clusters[position[csolution.size() - 1 + i]][position[csolution.size() - 1 + j]] - 1]) * 100));
                        }
                    }
                    handler.endArray();
                }
                handler.endArray();
                handler.endElement();
            }
        }
        );
        ArrayList<Integer> newSol = new ArrayList<>(n + 2);

        config.addCallback((opl) -> {
            try {
                IloIntMap x = opl.getElement("thisSubtour").asIntMap();
                int N = opl.getElement("N").asInt();
                int prev = 0;
                int next;
                for (int i = 0; i < N; i++) {
                    next = x.get(prev + 1) - 1;
                    newSol.add(prev);
                    prev = next;
                }
                //newSol.add(n + 1);
            } catch (IloException ex) {
                ex.printStackTrace();
            }
        });
        SolverStatus status = cplexBuilder.buildSolution(config);
        switch (status.getStatus()) {
            case SAT:
                break;
            case TIMEOUT_SINGLE:
                System.out.println("CPLEX ATSP MTZ " + status.getStatus());
                iotime += status.getElapsedTime() * 1e6;
                if (writeFileDat()) {
                    return true;
                }
                iotime += 3e11;
                break;
            default:
                System.out.println("CPLEX ATSP MTZ " + status.getStatus());
                iotime += status.getElapsedTime() * 1e6;
                return writeFileDat();
        }
        partialSolution = new ArrayList<>(newSol);
        partialSolution.remove(0);
  //          partialSolution.stream().forEach(i -> System.err.print(i+" "));
    //        System.err.println();
        for (int i = 0; i < partialSolution.size(); i++) {
            partialSolution.set(i, position[csolution.size() - 1 + partialSolution.get(i)]);
        }
        ArrayList<Integer> complete = new ArrayList<>(csolution.size() + partialSolution.size());
        complete.addAll(csolution);
        complete.addAll(partialSolution);
        ATSPsolution = new OptimizationProblemSolution(complete, fo);
//System.err.println(apply_tp_to_path(complete, 0, type_jam.z__));
   //    newSol.stream().forEach(i -> System.out.print(i+" "));
        return true;
    }

    public boolean writeFileDat2() throws IOException {
        if (n > 0) {
            TSPSolutionBuilder tspBuilder = new TSPSolutionBuilder(300000);
            SolverConfig config = new SolverConfig("model/tsp.mod");
            config.addCustomDataSource(new IloCustomOplDataSource(tspBuilder.getOplF()) {
                @Override
                public void customRead() {
                    IloOplDataHandler handler = getDataHandler();
                    handler.startElement("m");
                    handler.addIntItem(n + 2);
                    handler.endElement();

                    handler.startElement("dist1");
                    handler.startArray();
                    handler.startArray();
                    handler.addIntItem(-1000);
                    for (int j = 1; j < (n + 2); j++) {
                        if (costs[0][j] != 1000) {
                            if(Main.PREP && Main.FORK)
                                handler.addIntItem((int) ((costs[0][j] / new_uij[data.clusters[position[csolution.size() - 1 ]][position[csolution.size() - 1 + j]] - 1]) * 100));
                            else 
                                handler.addIntItem((int) ((apply_tp(getValuePartial(), costs[0][j], data.original_v_ij[data.clusters[position[csolution.size() - 1]][position[csolution.size() - 1 + j]] - 1], data.tp)) * 100));
                        } else {
                            handler.addIntItem(100000);
                        }
                    }
                    handler.endArray();
                    for (int i = 1; i < (n + 2); i++) {
                        handler.startArray();
                        for (int j = 0; j < (n + 2); j++) {
                            if (i == j) {
                                handler.addIntItem(-1000);
                            } //                    else if(i==0 && j<n+1)
                            //                        str.append((int)(Integer.MAX_VALUE)).append(" ");
                            else if (i == n + 1 && j > 0) {
                                handler.addIntItem(100000);
                            } else //str.append((int)((costs[i][j])*100)).append(" ");
                            {
                                handler.addIntItem((int) ((costs[i][j] / new_uij[data.clusters[position[csolution.size() - 1 + i]][position[csolution.size() - 1 + j]] - 1]) * 100));
                            }
                        }
                        handler.endArray();
                    }
                    handler.endArray();
                    handler.endElement();
                }
            }
            );
            ArrayList<Integer> newSol = new ArrayList<>(n + 2);

            config.addCallback((opl) -> {
                try {
                    IloIntMap x = opl.getElement("thisSubtour").asIntMap();
                    int N = opl.getElement("n").asInt();
                    int prev = 0;
                    int next;
                    for (int i = 0; i < N; i++) {
                        next = x.get(prev + 1) - 1;
                        if (prev < n + 1) {
                            newSol.add(prev);
                        }
                        prev = next;
                    }
                    newSol.add(n + 1);
                } catch (IloException ex) {
                    ex.printStackTrace();
                }
            });
            SolverStatus status = tspBuilder.buildSolution(config);
            if (!status.getStatus().equals(SolverStatus.Status.SAT)) {
                System.out.println("CPLEX ATSP MULTI " + status.getStatus());
                iotime += status.getElapsedTime() * 1e6;
                return writeFileDat1();
            }
            //System.out.println(status.getObjValue());
            partialSolution = new ArrayList<>(newSol);
            partialSolution.remove(0);
        } else {
            partialSolution = new ArrayList<>(1);
            partialSolution.add(n + 1);
        }        
       //     partialSolution.stream().forEach(i -> System.err.print(i+" "));
         //   System.err.println();
        for (int i = 0; i < partialSolution.size(); i++) {
            partialSolution.set(i, position[csolution.size() - 1 + partialSolution.get(i)]);
        }
        ArrayList<Integer> complete = new ArrayList<>(csolution.size() + partialSolution.size());
        complete.addAll(csolution);
        complete.addAll(partialSolution);
        ATSPsolution = new OptimizationProblemSolution(complete, fo);
        
        //System.err.println(apply_tp_to_path(complete, 0, type_jam.z__));
        //newSol.stream().forEach(i -> System.out.print(i+" "));
        return true;
    }

    public boolean writeFileDat3() throws IOException {
        if (n > 0) {
            CPLEXSolutionBuilder tspBuilder = new CPLEXSolutionBuilder();
            SolverConfig config = new SolverConfig("model/concorde.mod");
            config.addCustomDataSource(new IloCustomOplDataSource(tspBuilder.getOplF()) {
                @Override
                public void customRead() {
                    IloOplDataHandler handler = getDataHandler();
                    handler.startElement("m");
                    handler.addIntItem(n + 2);
                    handler.endElement();

                    handler.startElement("dist1");
                    handler.startArray();
                    handler.startArray();
                    handler.addIntItem(-1000);
                    for (int j = 1; j < (n + 2); j++) {
                        if (costs[0][j] != 1000) {
                            if(Main.PREP && Main.FORK)
                                handler.addIntItem((int) ((costs[0][j] / new_uij[data.clusters[position[csolution.size() - 1 ]][position[csolution.size() - 1 + j]] - 1]) * 100));
                            else 
                                handler.addIntItem((int) ((apply_tp(getValuePartial(), costs[0][j], data.original_v_ij[data.clusters[position[csolution.size() - 1]][position[csolution.size() - 1 + j]] - 1], data.tp)) * 100));
                        } else {
                            handler.addIntItem(100000);
                        }
                    }
                    handler.endArray();
                    for (int i = 1; i < (n + 2); i++) {
                        handler.startArray();
                        for (int j = 0; j < (n + 2); j++) {
                            if (i == j) {
                                handler.addIntItem(-1000);
                            } //                    else if(i==0 && j<n+1)
                            //                        str.append((int)(Integer.MAX_VALUE)).append(" ");
                            else if (i == n + 1 && j > 0) {
                                handler.addIntItem(100000);
                            } else //str.append((int)((costs[i][j])*100)).append(" ");
                            {
                                handler.addIntItem((int) ((costs[i][j] / new_uij[data.clusters[position[csolution.size() - 1 + i]][position[csolution.size() - 1 + j]] - 1]) * 100));
                            }
                        }
                        handler.endArray();
                    }
                    handler.endArray();
                    handler.endElement();
                }
            }
            );

            /*config.addCallback((opl) -> {
                try {
                    IloIntMap x = opl.getElement("thisSubtour").asIntMap();
                    int N = opl.getElement("n").asInt();
                    int prev = 0;
                    int next;
                    for (int i = 0; i < N; i++) {
                        next = x.get(prev + 1) - 1;
                        if (prev < n + 1) {
                            newSol.add(prev);
                        }
                        prev = next;
                    }
                    newSol.add(n + 1);
                } catch (IloException ex) {
                    ex.printStackTrace();
                }
            });*/
            long time = System.nanoTime();
            SolverStatus status = tspBuilder.buildSolution(config);
            System.exit(0);
            Process TSP = new ProcessBuilder(WDIR + "Concorde/concorde", "-N", "10", "instance.edg").directory(new File(WDIR + "Concorde")).start();
            //Process TSP = new ProcessBuilder("/utenti/calogiuri/TDTSP/PREPROCESSING/TSPsolver/atsp_750.exe").directory(new File("/utenti/calogiuri/TDTSP/PREPROCESSING/TSPsolver/")).start();
            StreamGobbler errorGobbler = new StreamGobbler(TSP.getErrorStream(), "ERROR");

            // any output?
            StreamGobbler outputGobbler = new StreamGobbler(TSP.getInputStream(), "OUTPUT");

            // kick them off
            errorGobbler.start();
            outputGobbler.start();
            time = System.nanoTime() - time;
            iotime += time;
            try {
                boolean exit = TSP.waitFor(5, TimeUnit.MINUTES);
                if (exit == false) {
                    System.out.println("ATSP No solution");
                    //System.out.println(str.toString());
                    TSP.destroyForcibly();
                    iotime += 3e11;
                    return writeFileDat2();
                }
            } catch (InterruptedException ex) {
                System.err.println(ex.getMessage());
            }
            partialSolution = new ArrayList<>(n + 2);
            File ris = new File("./Concorde/instance.sol");
            if (ris.exists()) {
                FileInputStream fis = new FileInputStream(ris);
                InputStreamReader isr = new InputStreamReader(fis);
                BufferedReader br = new BufferedReader(isr);
                String line = "";
                line = br.readLine();
                line = br.readLine();
                Arrays.stream(line.split("\\s+")).map(Integer::parseInt).filter(i -> i < n + 1).forEach(i -> partialSolution.add(i));                
            }            
            partialSolution.remove(0);
        } else {
            partialSolution = new ArrayList<>(1);
            partialSolution.add(n + 1);
        }        
       //     partialSolution.stream().forEach(i -> System.err.print(i+" "));
         //   System.err.println();
        for (int i = 0; i < partialSolution.size(); i++) {
            partialSolution.set(i, position[csolution.size() - 1 + partialSolution.get(i)]);
        }
        ArrayList<Integer> complete = new ArrayList<>(csolution.size() + partialSolution.size());
        complete.addAll(csolution);
        complete.addAll(partialSolution);
        ATSPsolution = new OptimizationProblemSolution(complete, fo);
        //System.err.println(apply_tp_to_path(complete, 0, type_jam.z__));
        //newSol.stream().forEach(i -> System.out.print(i+" "));
        return true;
    }
    
    @Override
    public double getInitialUB() {
        return data.tp[data.t - 1][1];
    }

    @Override
    public double getLBp() {
        return lbp;
    }

    @Override
    public Data getData() {
        return data;
    }

    public int getSize() {
        return (position.length);
    }

    public void stampaMatrice(double[][] matrix) {
        System.out.print("[");
        for (int i = 0; i < matrix.length; i++) {
            System.out.print("[");
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println("]");
        }
        System.out.print("]");
    }

    public void stampaVettore(int[] matrix) {
        System.out.print("[");
        for (int i = 0; i < matrix.length; i++) {
            System.out.print(matrix[i] + " ");
        }
        System.out.println("]");
    }

    @Override
    public void prova() {
        ArrayList<Integer> TD = new ArrayList<Integer>(n + 2);
        ArrayList<Integer> statico = new ArrayList<Integer>(n + 2);
        int[] TDarr = {0, 5, 15, 9, 19, 12, 11, 16, 18, 14, 3, 4, 20, 6, 10, 1, 2, 17, 13, 8, 7, 21};
        int[] staticoarr = {0, 17, 2, 5, 15, 9, 19, 12, 11, 16, 18, 14, 3, 4, 20, 6, 10, 1, 13, 8, 7, 21};
        for (int i = 0; i < n + 2; i++) {
            TD.add(TDarr[i]);
            statico.add(staticoarr[i]);
        }
        double td1 = apply_tp_to_path(TD, 0, TDTSP.type_jam.z);
        double statico1 = apply_tp_to_path(statico, 0, TDTSP.type_jam.z);
        //System.out.println(td1+"-"+statico1);
        //System.exit(0);
    }

}
