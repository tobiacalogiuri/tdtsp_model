/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bandbtsp;

import java.util.ArrayList;

/**
 *
 * @author user
 */
class OptimizationProblemSolution {
    ArrayList<Integer> sol;
    private double lb;
    
    public OptimizationProblemSolution(ArrayList<Integer> sol1){
        sol = sol1;
    }
    
    public OptimizationProblemSolution(ArrayList<Integer> sol1, double cost1){
        sol = sol1;
        lb = cost1;
    }
    
    public double getLb() {
        return lb;
    }

    public void setLb(double lb) {
        this.lb = lb;
    }
    
    public ArrayList<Integer> getSol(){
        return sol;
    }
    
    public void setSol(ArrayList<Integer> sol){
        this.sol = sol;
    }
    
}
