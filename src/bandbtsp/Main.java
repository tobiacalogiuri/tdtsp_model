/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bandbtsp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 *
 * @author user
 */
public class Main {

    public static boolean FIRST_SOL = true;
    public static boolean PREP = true;
    public static boolean FORK = false;
    
    public static void main(String[] args) throws IOException {
        //System.setProperty("java.library.path", "/home/tommaso/bin/CPLEX_Studio127/opl/bin/x86-64_linux");
        //System.loadLibrary("libopl1270");
        /*args=new String[]{
            "instances/15_K2_10.txt","instances/PatternA/Jams", "true", "false"
        };*/
        if(args.length != 4) {
            System.err.println("Wrong arguments");
            return;
        }
        PREP = Boolean.parseBoolean(args[2]);
        FORK = Boolean.parseBoolean(args[3]);
        File f = new File(args[0]), f2 = new File(args[1]);        
        OptimizationProblem tdtsp = new TDTSP();
        tdtsp.readInstanceTW(f, f2);
        BandBTSP beb = new BandBTSP(tdtsp);
        beb.solve(f.toString());
        System.out.println("Pattern " + f2.getName() + " - " + f.getName());
    }
    
    
    public static void main2(String[] args) {
        //PREP = args.length > 0 && "PREPROCESSING".equals(args[0]);
        File file = new File("./instances");
        File file2 = new File("./instances/PatternA");
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("Output.txt", true)))) {
            out.println("PatternA");
            out.println("Istanza;z_best;Nodi;tempo_esecuzione;Gap_i;Gap_f;zstatico/zottimo;Ottima");
        } catch (IOException e) {
        }
        try (PrintWriter log = new PrintWriter(new BufferedWriter(new FileWriter("Log.txt", true)))) {
            log.println("PatternA");
        } catch (IOException e) {
        }
        for (File f2 : file2.listFiles()) {
            try {
                for (File f : Stream.of(file.listFiles()).sorted((ff1, ff2) -> {
                    return ff1.getName().compareToIgnoreCase(ff2.getName());
                }).toArray(File[]::new)) {
                    if (f.isDirectory()) {
                        continue;
                    }
                    if (!f2.toString().contains("Jams")) {
                        String out2 = f2.toString();
                        out2 = out2.substring(out2.length() - 7);
                        System.out.println(out2);
                        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("Output.txt", true)))) {
                            out.println(out2);
                        } catch (IOException e) {
                        }
                        try (PrintWriter log = new PrintWriter(new BufferedWriter(new FileWriter("Log.txt", true)))) {
                            log.println(out2);
                        } catch (IOException e) {
                        }
                        f2 = new File(f2, "Jams");
                    }
                    //System.out.print(f.toString()+";");
                    OptimizationProblem tdtsp = new TDTSP();
                    tdtsp.readInstanceTW(f, f2);
                    BandBTSP beb = new BandBTSP(tdtsp);
                    beb.solve(f.toString());
                    System.out.println("PatternA - " + f.toString());
                    //Da levare prima o poi
                    //break;
                }
                //Da levare prima o poi
                //break;
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        file2 = new File("./instances/PatternB");
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("Output.txt", true)))) {
            out.println("PatternB");
            out.println("Istanza;z_best;Nodi;tempo_esecuzione;Gap_i;Gap_f;zstatico/zottimo;Ottima");
        } catch (IOException e) {
        }
        try (PrintWriter log = new PrintWriter(new BufferedWriter(new FileWriter("Log.txt", true)))) {
            log.println("PatternB");
        } catch (IOException e) {
        }
        for (File f2 : file2.listFiles()) {
            try {
                for (File f : Stream.of(file.listFiles()).sorted((ff1, ff2) -> {
                    return ff1.getName().compareToIgnoreCase(ff2.getName());
                }).toArray(File[]::new)) {
                    if (f.isDirectory()) {
                        continue;
                    }
                    if (!f2.toString().contains("Jams")) {
                        String out2 = f2.toString();
                        out2 = out2.substring(out2.length() - 7);
                        System.out.println(out2);
                        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("Output.txt", true)))) {
                            out.println(out2);
                        } catch (IOException e) {
                        }
                        try (PrintWriter log = new PrintWriter(new BufferedWriter(new FileWriter("Log.txt", true)))) {
                            log.println(out2);
                        } catch (IOException e) {
                        }
                        f2 = new File(f2, "Jams");
                    }
                    //System.out.print(f.toString()+";");
                    OptimizationProblem tdtsp = new TDTSP();
                    tdtsp.readInstanceTW(f, f2);
                    BandBTSP beb = new BandBTSP(tdtsp);
                    beb.solve(f.toString());
                    System.out.println("PatternB - " + f.toString());
                    //Da levare prima o poi
                    //break;
                }
                //Da levare prima o poi
                //break;
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
