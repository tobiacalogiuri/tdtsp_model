/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bandbtsp;

/**
 *
 * @author user
 */
import static bandbtsp.OptimizationProblemComparison.SENIORITY_ORDER;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
//import com.ms.util.VectorSort;

public class BandBTSP {

    /**
     * @param args the command line arguments
     */
    private OptimizationProblem P;
    private double U;
    private OptimizationProblem currentbest = null;
    private PriorityQueue<OptimizationProblem> activeproblems;
    static double M = Double.MAX_VALUE / 1000;
    private long nodesGenerated = 0;
    private double elapsedTime = 0;
    private OptimizationProblemComparison opc;

    public BandBTSP(OptimizationProblem problem) {
        this.P = problem;
        int n = P.getProblemSize();
        activeproblems = new PriorityQueue<>(n+1, SENIORITY_ORDER);
        activeproblems.add(P);
        U = problem.getInitialUB();
        //Fatto con le lambda, dovrebbe andare...
        //this.opc = new OptimizationProblemComparison();
    }

    public void setU(double U) {
        this.U = U;
    }

    public OptimizationProblem solve(String instanceName) {
        OptimizationProblem Pi;
        OptimizationProblem Ri;
        double z_;
        double z=0;
        double z_i=0;
        double GAPi = 0.0;
        long d0 = System.nanoTime();
        long ioTime = 0L; 
        long hour = 3600000000000L;
        boolean primo = true;
        boolean optimal = true;
        int counter = 0;
        OptimizationProblem root = new TDTSP();
        Main.FIRST_SOL = Main.PREP;
        while (activeproblems.size() > 0) {
            counter++;
            Pi = selectProblem();
            if(Main.FIRST_SOL) {
                Ri = Pi.getRelaxation(U, false);
                U = Ri.getValue();
                Main.FIRST_SOL = false;
            }
            Ri = Pi.getRelaxation(U, primo);
            if(Ri.getValue__()==Double.MAX_VALUE) 
                continue;
            z = Ri.getValue();
            z_ = Ri.getValue_();
            if(primo){
                U = z;
                primo = false;
                z_i = z;
                root = Ri;
                GAPi = (z-z_)/z_;
                currentbest = root;
            }
            //System.out.println(""+Ri.getProgress());
            
            //System.out.println(Ri.getProgress()+" - "+" "+Ri.getSolution().sol.toString()+" z="+z+" z_="+z_+" lbp="+Ri.getLBp()+" U="+U);
//            if(Ri.getSolution().sol.size()<Ri.getProblemSize())
//                continue;

            if (z_ < U) {
                //System.out.println(""+Ri.getSolution().sol.toString()+" z="+z+" z_="+z_);
                if(z < U) { U = z; currentbest = Ri; }
                if (z==z_ && z!=0) {
                        U = z;
                        currentbest = Ri;
                } 
                //else{
                if(!(z==z_ && z!=0)) {
                    OptimizationProblem[] subPs
                            = Ri.branch(Ri.getValuePartial(), U);
                    for(int k = 0; k < subPs.length; k++) {
                        this.activeproblems.add(subPs[k]);
                        this.nodesGenerated++;
                    }
               }
            }
             long d2 = System.nanoTime();
             ioTime += Ri.getIotime();
             if(((d2 - d0 - ioTime))> hour){
                 optimal = false;
                 break;
             }
        } // of while(non-empty)
        long d1 = System.nanoTime();
        
        this.elapsedTime
                = (double) ((d1 - d0 - ioTime) / 1000000000.0);
        z= currentbest.getValue();
        // T&T: il gap finale dovrebbe essere calcolato rispetto al lower bound z_ e non rispetto al quick lower bound: causa dei gapF più alti dei gapI
        double GAPf = 0;
        if(!optimal){
            z_ = U;
            while (activeproblems.size() > 0) {
                Pi = selectProblem();
                if(Pi.getLBp()<U && z_>Pi.getLBp()) z_ = Pi.getLBp();
            }
            GAPf = (z-z_)/z_;
        }
        try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("Output.txt", true)))) {
            out.println(instanceName+";"+z+";"+this.nodesGenerated+";"+this.elapsedTime+";"+GAPi+";"+GAPf+";"+ (z_i/z)+";"+optimal);
        }catch (Exception e) {
            return null;
        }
        try(PrintWriter log = new PrintWriter(new BufferedWriter(new FileWriter("Log.txt", true)))) {
            log.println("Istanza: "+ instanceName);
            log.println("Best solution ("+z+")"+currentbest.getSolution().sol.toString());
            log.println("Static solution("+z_i+")"+root.getSolution().sol.toString());
        }catch (Exception e) {
            return null;
        }
        //currentbest.apply_tp_to_pathTEST(currentbest.getSolution().sol, 0, TDTSP.type_jam.z);
        return currentbest;
    } // of solve

    private OptimizationProblem selectProblem() {
        OptimizationProblem selected;
        selected
                = (OptimizationProblem) this.activeproblems.poll();
        
        /*for(OptimizationProblem p: this.activeproblems)
            System.out.println(p.getLBp());
        System.out.println("selezionato " + selected.getLBp());*/
        return selected;
    } // of selectProblem()

    public double getElapsedTime() {
        return this.elapsedTime;
    }

    public long getNodeCount() {
        return this.nodesGenerated;
    }
} // of class
