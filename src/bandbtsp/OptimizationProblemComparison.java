/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bandbtsp;

import java.util.Comparator;

/**
 *
 * @author user
 */
public class OptimizationProblemComparison
//implements com.ms.util.Comparison
{
    /// prendiamo prima quello con LB più grande!!!
    static final Comparator<OptimizationProblem> SENIORITY_ORDER = 
        (OptimizationProblem o1, OptimizationProblem o2) -> {
            if(o1.getLBp()<o2.getLBp()) return 1;
            if(o1.getLBp()>o2.getLBp()) return -1;
//            if(o1.getProgress()<o2.getProgress()) return 1;
//            if(o1.getProgress()>o2.getProgress()) return -1;
//            if(o1.getSolution().getLb()<o2.getSolution().getLb()) return 1;
//            if(o1.getSolution().getLb()>o2.getSolution().getLb()) return -1;
            return 0;
    };
} // of class
