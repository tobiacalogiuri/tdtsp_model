/*
 * Copyright 2014 Tommaso.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cplex;



/**
 * This class provides informations about a solving process.
 * @author Tommaso
 */
public class SolverStatus {
    
    
    public enum Status { SAT, UNSAT, TIMEOUT, TIMEOUT_SINGLE };
    
    private Status status;
    private Double objValue;
    private long elapsedTime;
    
    public SolverStatus() {}

    public SolverStatus(Status status, Double objValue) {
        this.status = status;
        this.objValue = objValue;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Double getObjValue() {
        return objValue;
    }

    public void setObjValue(Double objValue) {
        this.objValue = objValue;
    }

    public long getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(long elapsedTime) {
        this.elapsedTime = elapsedTime;
    }
    
    public void report() {
        switch(status) {
            case SAT:
                System.out.println("Objective: " + objValue);
                break;
            case UNSAT:
                System.out.println("No solution");
                break;
            case TIMEOUT:
                System.out.println("TIMEOUT");
                break;
        }
        
    }   
}
