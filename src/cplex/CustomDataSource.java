/*
 * Copyright 2014 Tommaso.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cplex;

import ilog.opl.IloCustomOplDataSource;
import ilog.opl.IloOplDataHandler;
import ilog.opl.IloOplFactory;

/**
 * This class is used to provide an initial solution to a model (from a Model object)
 * 
 * @author Tommaso
 */
public class CustomDataSource extends IloCustomOplDataSource {

    private final Object object;
    
    public CustomDataSource(IloOplFactory oplF, Object object) {
        super(oplF);
        this.object = object;
    }
    
    @Override
    public void customRead() {
        IloOplDataHandler handler = getDataHandler();
        
        
        /*model.getVariables().forEach((k,v) -> {
            boolean flag = false;
            for(EntitySet set : v.getEntitySets()) 
                flag |= model.getInterestEntitySets().contains(set);
            if(!flag || v.getValues().isEmpty()) return;
            
            int N = v.getEntitySets().size();
            int[] factors = new int[N];
            factors[N-1] = v.getEntitySets().get(N-1).getCardinality();
            
            for(int i = N-2; i >= 0; i --) {
                factors[i] = factors[i+1] * v.getEntitySets().get(i).getCardinality();
            }
            
            handler.startElement(k + Commons.VAR_SUFFIX);
            int index = 0;
            if(v.getType().isInt()) {
                for(Double value : v.getValues()) {
                    for(int i = 0; i < N; i++)
                        if(index % factors[i] == 0)
                            handler.startArray();

                    handler.addIntItem((int)Math.round(value));

                    for(int i = 0; i < N; i++)
                        if(index % factors[i] == factors[i]-1)
                            handler.endArray();
                    index ++;
                }
            } else {
                for(Double value : v.getValues()) {
                    for(int i = 0; i < N; i++)
                        if(index % factors[i] == 0)
                            handler.startArray();

                    handler.addNumItem(value);

                    for(int i = 0; i < N; i++)
                        if(index % factors[i] == factors[i]-1)
                            handler.endArray();
                    index ++;
                }
            }
            handler.endElement();
        });
        */
        
    }
}
