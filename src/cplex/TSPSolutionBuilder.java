/*
 * Copyright 2014 Tommaso.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cplex;

import ilog.concert.IloException;
import ilog.concert.IloTupleBuffer;
import ilog.concert.cppimpl.JavaToCppInputStreamAdapter;
import ilog.cplex.IloCplex;
import ilog.opl.IloCustomOplDataSource;
import ilog.opl.IloOplDataElements;
import ilog.opl.IloOplDataHandler;
import ilog.opl.IloOplDataSource;
import ilog.opl.IloOplFactory;
import ilog.opl.IloOplModel;
import ilog.opl.IloOplModelDefinition;
import ilog.opl.IloOplModelSource;
import ilog.opl.IloOplProject;
import ilog.opl.IloOplRunConfiguration;
import ilog.opl.IloOplSettings;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

/**
 * This class is a wrapper used to configure a CPLEX-solver run using the
 * settings specified into a SolverConfig object.
 *
 * @see it.unisalento.modelandrun.beans.SolverConfig
 * @author Tommaso
 *
 *
 */
public class TSPSolutionBuilder {

    private final static String MODEL;
    private final int timeLimit;
    static {
        StringBuilder b = new StringBuilder("");
        try(BufferedReader reader = new BufferedReader(new FileReader(new File("model/tsp.mod")))) {            
            String s;            
            while((s=reader.readLine())!=null)
                b.append(s).append("\n");            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        MODEL = b.toString();        
    }
    
    private final IloOplFactory oplF;

    static {
        IloOplFactory.setDebugMode(false);
        IloOplFactory.setDebugModeWarning(false);
    }
     
    public TSPSolutionBuilder(int limit) {        
        oplF = new IloOplFactory();
        oplF.setOut(null);
        oplF.setWarning(null);
        oplF.setError(null);    
        this.timeLimit = limit;
    }
    //private static Map<String, IloOplDataSource> CACHE = new HashMap<>();
    //private static IloOplFactory oplF = new IloOplFactory();

    public SolverStatus buildSolution(SolverConfig config) {
        if (config == null) {
            return null;
        }
        SolverStatus status = new SolverStatus();
        config.addCustomDataSource(new IloCustomOplDataSource(oplF) {
            @Override
            public void customRead() {
                IloOplDataHandler handler = getDataHandler();
                handler.startElement("subtours");
                handler.startSet();
                handler.endSet();
                handler.endElement();
            }
        }
        );

        try {
            IloOplSettings settings = oplF.createOplSettings(oplF.createOplErrorHandler(System.err));
            IloOplModelSource modelSource = oplF.createOplModelSourceFromString(MODEL, "modello");
            IloOplModelDefinition def = oplF.createOplModelDefinition(modelSource, settings);
            IloCplex cplex = oplF.createCplex();
            cplex.setOut(null);
            cplex.setError(null);
            cplex.setWarning(null);
            IloOplModel opl = oplF.createOplModel(def, cplex);

            for (String dat : config.getDatsPath()) {
                //if(CACHE.containsKey(dat))
                //  opl.addDataSource(CACHE.get(dat));
                //else {
                IloOplDataSource dataSource = oplF.createOplDataSource(dat);
                opl.addDataSource(dataSource);
                //  CACHE.put(dat, dataSource);
                //}
            }
            for (IloOplDataSource ds : config.getCustomDataSources()) {
                opl.addDataSource(ds);
            }

            if (config.getSettingsPath() != null) {
                try (FileInputStream opsStream = new FileInputStream(config.getSettingsPath())) {
                    JavaToCppInputStreamAdapter streamAdapter = new JavaToCppInputStreamAdapter(opsStream);
                    String again = "again" + config.getSettingsPath();
                    IloOplProject.ApplyProjectSettings(streamAdapter, again, opl);
                }
            }
            for (ISolverCallback c : config.getPrepareCallbacks()) {
                c.doAction(opl);
            }

            if (config.getMipStartPath() != null) {
                //cplex.readMIPStart(config.getMipStartPath());
            }

            opl.generate();

            IloOplDataElements masterDataElements = opl.makeDataElements();
            
            long start = System.currentTimeMillis();
            
            while (true) {
                cplex.clearModel();
                IloOplRunConfiguration oplConf = oplF.createOplRunConfiguration(def, masterDataElements);
                oplConf.setCplex(cplex);
                IloOplModel opl1 = oplConf.getOplModel();
                //cplex.setParam(IloCplex.IntParam.Threads, 1);
                //cplex.setParam(IloCplex.DoubleParam.TiLim, 8);
                opl1.generate();
                boolean solved = cplex.solve();
                if (solved) {
                    if (config.getMipStartPath() != null) {
                        //cplex.writeMIPStart(config.getMipStartPath());
                    }
                    // logger.log(Level.INFO, "Objective: " + cplex.getObjValue());
                    if (config.getOutPath() != null) {
                        try (PrintStream pout = new PrintStream(config.getOutPath())) {
                            opl1.printSolution(pout);
                        }
                    }

                    opl1.postProcess();
                    status = new SolverStatus(SolverStatus.Status.SAT, cplex.getObjValue());

                    int ns = opl1.getElement("newSubtourSize").asInt();
                    int n = opl1.getElement("n").asInt();
                    if (ns == n) {
                        if (config.getCallbacks() != null) {
                            for (ISolverCallback callback : config.getCallbacks()) {
                                callback.doAction(opl1);
                            }
                        }
                        opl1.end();
                        cplex.end();
                        break;
                    }
                        //masterDataElements = opl1.makeDataElements();
                    IloTupleBuffer buf = masterDataElements.getElement("subtours").asTupleSet().makeTupleBuffer(-1);
                    buf.setIntValue("size", ns);
                    buf.setIntMapValue("subtour", opl1.getElement("newSubtour").asIntMap());
                    buf.commit();
                } else {
                    status = new SolverStatus();
                    status.setObjValue(null);
                    if (IloCplex.Status.Infeasible.equals(cplex.getStatus())) {
                        status.setStatus(SolverStatus.Status.UNSAT);
                    } else {
                        status.setStatus(SolverStatus.Status.TIMEOUT_SINGLE);
                    }
                    cplex.end();
                    break;
                }
                long end = System.currentTimeMillis();                
                if(end - start > timeLimit) {
                    status.setObjValue(null);
                    status.setStatus(SolverStatus.Status.TIMEOUT);
                    break;
                }                    
                //opl1.end();
                //cplex.end();
                //oplConf.end();
            }
            status.setElapsedTime(System.currentTimeMillis() - start);
            opl.end();
            cplex.end();
            oplF.end();
        } catch (IloException | IOException ex) {
            System.out.println("" + ex.getMessage());
            ex.printStackTrace();
        }
        return status;
    }

    public IloOplFactory getOplF() {
        return oplF;
    }
}
