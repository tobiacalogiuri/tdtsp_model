/*
 * Copyright 2014 Tommaso.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cplex;

import ilog.opl.IloCustomOplDataSource;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * This class defines the configurations needed to run the CPLEX or CP solver.
 * 
 * @author Tommaso
 */
public class SolverConfig implements Serializable {
    
    private String modPath;
    private Collection<String> datsPath;
    private String settingsPath;
    private String outPath;
    private String mipStartPath;
    /**
     * a list of callback actions which will be called before solving 
     */
    private final List<ISolverCallback> prepareCallbacks = new LinkedList<>();
    /**
     * a list of callback actions which will be called after solving (if the solve process ends successfully)
     */
    private final List<ISolverCallback> callbacks = new LinkedList<>();
    /**
     * a list of user-defined data sources
     */
    private final List<IloCustomOplDataSource> customDataSources = new LinkedList<>();

    public SolverConfig() {}
    
    public SolverConfig(String modPath, Collection<String> datsPath, String settingsPath, String outPath) {
        this.modPath = modPath;
        this.datsPath = datsPath;
        this.settingsPath = settingsPath;
        this.outPath = outPath;
    }
    
    public SolverConfig(String modPath, Collection<String> datsPath, String settingsPath) {
        this(modPath, datsPath, settingsPath, null);
    }
    
    public SolverConfig(String modPath, Collection<String> datsPath) {
        this(modPath, datsPath, null, null);
    }
    
    public SolverConfig(String modPath, String... datsPath) {
        this(modPath, Arrays.asList(datsPath));
    }

    public String getModPath() {
        return modPath;
    }

    public void setModPath(String modPath) {
        this.modPath = modPath;
    }

    public Collection<String> getDatsPath() {
        return datsPath;
    }

    public void setDatsPath(Collection<String> datsPath) {
        this.datsPath = datsPath;
    }

    public String getSettingsPath() {
        return settingsPath;
    }

    public void setSettingsPath(String settingsPath) {
        this.settingsPath = settingsPath;
    }

    public String getOutPath() {
        return outPath;
    }

    public void setOutPath(String outPath) {
        this.outPath = outPath;
    }

    public List<ISolverCallback> getCallbacks() {
        return callbacks;
    }

    /**
     * Add a callback action which will be executed after a successful solve process
     * @param e
     * @return true
     */
    public boolean addCallback(ISolverCallback e) {
        return callbacks.add(e);
    }

    /**
     * Add a callback action which will be executed before the solve process
     * @param e     
     * @return      
     */
    public boolean addPrepareCallback(ISolverCallback e) {
        return prepareCallbacks.add(e);
    }
    
    /**
     * Add a user-defined data source
     * @param e     
     * @return      
     */
    public boolean addCustomDataSource(IloCustomOplDataSource e) {
        return customDataSources.add(e);
    }

    public List<IloCustomOplDataSource> getCustomDataSources() {
        return customDataSources;
    }

    public List<ISolverCallback> getPrepareCallbacks() {
        return prepareCallbacks;
    }
    
    public String getMipStartPath() {
        return mipStartPath;
    }

    public void setMipStartPath(String mipStartPath) {
        this.mipStartPath = mipStartPath;
    }
    
}
