/*
 * Copyright 2014 Tommaso.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cplex;

import ilog.concert.IloException;
import ilog.concert.cppimpl.JavaToCppInputStreamAdapter;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.CplexStatus;
import ilog.opl.IloOplDataSource;
import ilog.opl.IloOplFactory;
import ilog.opl.IloOplModel;
import ilog.opl.IloOplModelDefinition;
import ilog.opl.IloOplModelSource;
import ilog.opl.IloOplProject;
import ilog.opl.IloOplSettings;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;


/**
 * This class is a wrapper used to configure a CPLEX-solver run using the settings 
 * specified into a SolverConfig object.
 * 
 * @see it.unisalento.modelandrun.beans.SolverConfig
 * @author Tommaso
 * 
 * 
 */
public class CPLEXSolutionBuilder {
    
    private final IloOplFactory oplF;
    /*
    static {
        IloOplFactory.setDebugMode(false);
    }
    */
    public CPLEXSolutionBuilder() {
        oplF = new IloOplFactory();     
        oplF.setOut(System.out);
        oplF.setWarning(System.err);
        oplF.setError(System.err);
    }
    //private static Map<String, IloOplDataSource> CACHE = new HashMap<>();
    //private static IloOplFactory oplF = new IloOplFactory();
    
    public SolverStatus buildSolution(SolverConfig config) {
        if(config == null) return null;
        SolverStatus status = null;
        try {
            IloOplSettings settings = oplF.createOplSettings(oplF.createOplErrorHandler(System.err));
            IloOplModelSource modelSource = oplF.createOplModelSource(config.getModPath());
            IloOplModelDefinition def = oplF.createOplModelDefinition(modelSource, settings);
            IloCplex cplex = oplF.createCplex();
            cplex.setOut(null);
            IloOplModel opl = oplF.createOplModel(def, cplex);
            
            for(String dat : config.getDatsPath()) {
                //if(CACHE.containsKey(dat))
                  //  opl.addDataSource(CACHE.get(dat));
                //else {
                IloOplDataSource dataSource = oplF.createOplDataSource(dat);
                opl.addDataSource(dataSource);
                  //  CACHE.put(dat, dataSource);
                //}
            }
            for(IloOplDataSource ds: config.getCustomDataSources())
                opl.addDataSource(ds);
            
            if(config.getSettingsPath() != null) {
                try (FileInputStream opsStream = new FileInputStream(config.getSettingsPath())) {
                    JavaToCppInputStreamAdapter streamAdapter = new JavaToCppInputStreamAdapter(opsStream);
                    String again = "again" + config.getSettingsPath();
                    IloOplProject.ApplyProjectSettings(streamAdapter, again, opl);
                }
            }
            //cplex.setParam(IloCplex.IntParam.WriteLevel, 1);
            opl.generate();
            
            for(ISolverCallback c: config.getPrepareCallbacks())
                c.doAction(opl);
            
            /*if(config.getMipStartPath() != null)
                cplex.readMIPStart(config.getMipStartPath());*/
            //cplex.setParam(IloCplex.IntParam.IntSolLim, 1);
            
            //cp.setParameter(IloCP.DoubleParam.TimeLimit, 1);
            //cp.setParameter(IloCP.IntParam.TemporalRelaxation, 1);
            //cplex.setParam(IloCplex.DoubleParam.TiLim, Config.getInstance().getIntProperty("time_limit"));
            //cplex.setParam(IloCplex.DoubleParam.TreLim, 10240);
            //cplex.solutionGoal(null, null).
            
            long start = System.currentTimeMillis();
            boolean solved = cplex.solve();
            long end = System.currentTimeMillis();
            if (solved) {
                //if(config.getMipStartPath() != null)
                    //cplex.writeMIPStart(config.getMipStartPath());
                // logger.log(Level.INFO, "Objective: " + cplex.getObjValue());
                if(config.getOutPath() != null) 
                    try(PrintStream pout = new PrintStream(config.getOutPath())) {
                        opl.printSolution(pout);
                    }
                
                opl.postProcess();                
                status = new SolverStatus(CplexStatus.AbortTimeLim.equals(cplex.getCplexStatus())? SolverStatus.Status.TIMEOUT_SINGLE: SolverStatus.Status.SAT, cplex.getObjValue());
                
                if(config.getCallbacks() != null)
                    for(ISolverCallback callback : config.getCallbacks())
                        callback.doAction(opl);
            } else {
                status = new SolverStatus();
                status.setObjValue(null);
                if(IloCplex.Status.Infeasible.equals(cplex.getStatus())) {
                    status.setStatus(SolverStatus.Status.UNSAT);
                }
                else { 
                    status.setStatus(SolverStatus.Status.TIMEOUT);
                }
            }
            status.setElapsedTime(end-start);
            //opl.end();
            cplex.end();
            oplF.end();
        } catch (IloException | IOException ex) {
            System.out.println(""+ex.getMessage());
        }
        return status;
    }

    public IloOplFactory getOplF() {
        return oplF;
    }
}
