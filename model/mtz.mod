/*********************************************
 * OPL 12.6.0.0 Model
 * Author: Tommy
 * Creation Date: 19/apr/2017 at 12:06:48
 *********************************************/
int N = ...;

range VERTICI = 1..N;
//int dist1[1..N+1][1..N+1] = ...;
int dist[i in VERTICI][j in VERTICI] = ...;

execute {
	cplex.threads=1;
	cplex.tilim=300.0;
}

dvar boolean x[VERTICI][VERTICI];
dvar int+ u[VERTICI];

minimize
  sum( i in VERTICI, j in VERTICI : j != i) (dist[i][j]*x[i][j]);

subject to {
  forall( i in VERTICI )
    Degree1:
      sum( j in VERTICI : j != i ) 
        x[i][j] == 1;
  forall( i in VERTICI )
    Degree2:
      sum( j in VERTICI : i != j ) 
        x[j][i] == 1;
  forall( i in VERTICI, j in VERTICI : i != j && i >= 2 && j >= 2 )
    NoSubtour1:
      u[i] - u[j] + N*x[i][j] <= N - 1;
  forall( i in VERTICI :  i >= 2 )
    NoSubtour2:
      u[i] <= N;
  forall( i in VERTICI : i >= 2 )
    NoSubtour3:
      u[i] >= 2;
  NoSubtour4:
      u[1] == 1;
};

int thisSubtour[i in VERTICI] = sum(j in VERTICI) j*x[i][j];

execute {
	thisSubtour;
}
/*
execute {
	var f = new IloOplOutputFile("../../temp/initialSolution.dat");
	f.writeln("x_0 = ", x, ";");
	f.writeln("u_0 = ", u, ";");
	f.close();
};*/
 