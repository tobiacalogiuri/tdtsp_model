/*********************************************
 * OPL 12.7.0.0 Model
 * Author: tommaso
 * Creation Date: 28/set/2017 at 11:05:03
 *********************************************/
/*****************************************************************************
 *
 * DATA
 *
 *****************************************************************************/

// Cities
int     m       = ...;
int n = 2*m;
int dist1[1..m][1..m] = ...;

execute {
  dist1[1][m] = 100000;
}

range   Cities  = 1..n;

// Edges -- sparse set
tuple       edge        {int i; int j;}
setof(edge) Edges       = {<i,j> | ordered i,j in Cities: (i > m && j <= m) || (i <=m && j > m)};
int         dist[<i,j> in Edges] = i>j? dist1[i-m][j]: dist1[j-m][i];

int countE = sum(<i,j> in Edges) (dist[<i,j>] < 100000);

execute {
  var f = new IloOplOutputFile("../Concorde/instance.edg");
  f.writeln(n, " ", countE);
  for(var a in Edges) {
  	if(dist[a] < 100000) {
        f.writeln(a.i-1, " ", a.j-1, " ", dist[a]);
  	}
  }

  f.close();
}
