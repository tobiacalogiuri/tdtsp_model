/*********************************************
 * OPL 12.6.0.0 Model
 * Author: user
 * Creation Date: 20/dic/2016 at 13:03:26
 *********************************************/

 int  cluster = ...;
 int hTime = ...;
 range c = 1..cluster;
 range h = 1..hTime;
 float v[c][h] = ...;
 float v_max[c] = ...;
 float v_max1[i in c] = max(j in h) v[i][j];
 float length[c] = ...;
 float total_length = ...;
 float u_max = ...;
 float u_max1= max(i in c) v_max1[i];
 
 dvar float+ y[c][h];
 dvar float+ b_primo[h];
 dvar float+ u[c];
 
 minimize sum(i in c, j in h) y[i][j]*(length[i]/total_length);
 
 subject to{
 	forall(i in c, j in h){
 		u[i]-y[i][j] == v[i][j]*b_primo[j];
 	}
 	forall(i in c){
 		u[i] >= v_max1[i];
		//u[i] <= u_max1;
 	}
	forall(j in h){
		b_primo[j] >= 1;
	}
 }
 
float d_[i in c][j in h] = v[i][j]*b_primo[j]/u[i];
float obj2 = sum(i in c, j in h) length[i]*d_[i][j]/total_length;
/*float b[j in h] = 1./b_primo[j];

execute {
	writeln("u=",u);
	writeln("vmax=",v_max1);

	writeln("b=", b);

	writeln("d=", d_);
}*/