/*********************************************
 * OPL 12.6.0.0 Model
 * Author: user
 * Creation Date: 20/dic/2016 at 13:03:26
 *********************************************/

 int  cluster = ...;
 int hTime = ...;
 range c = 1..cluster;
 range h = 1..hTime;
 float v[c][h] = ...;
 float v_max[c] = ...;
 float length[c] = ...;
 float total_length = ...;
 float u_max = ...;
 
 dvar float+ y[c][h];
 dvar float+ b[h];
 dvar float+ u_primo[c];
 
 minimize sum(i in c, j in h) y[i][j]*(length[i]/total_length);
 
 subject to{
 	forall(i in c, j in h){
 		b[j]-y[i][j] == v[i][j]*u_primo[i];
 		b[j] >= 0;
 		b[j] <= 1;
 	}
 	forall(i in c){
 		u_primo[i] <= 1/v_max[i];
 		u_primo[i]>=u_max;
 	}
 	
 }
 
float d_[i in c][j in h] = v[i][j]*u_primo[i]/b[j];
float obj2 = sum(i in c, j in h) length[i]*d_[i][j]/total_length;